import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
// import { AuthenticationGuard } from './Services/auth/authentication.guard';
import { AngularFireAuthGuard, redirectLoggedInTo } from '@angular/fire/auth-guard';
// import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { redirectUnauthorizedTo, canActivate } from '@angular/fire/auth-guard';
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const redirectLoggedInToSendEmail = () => redirectLoggedInTo(['side-menu']);


const routes: Routes = [
  { path: '', component: RegisterComponent },
  { path: 'login', component: LoginComponent},
  { path: 'verify-email', component: VerifyEmailComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  {
    path: 'side-menu', component: SideMenuComponent,  canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin },
    children: [
      { path: "traveller", outlet: "sidebar", loadChildren: () => import(`./traveller-management/traveller-management.module`).then(m => m.TravellerManagementModule) },
      { path: "onboarding", outlet: "sidebar", loadChildren: () => import(`./onboarding-management/onboarding-management.module`).then(m => m.OnboardingManagementModule) },
      { path: "aboutus", outlet: "sidebar", loadChildren: () => import(`./about-us-management/about-us-management.module`).then(m => m.AboutUsManagementModule) },
      { path: "riders", outlet: "sidebar", loadChildren: () => import(`./rider-management/rider-management.module`).then(m => m.RiderManagementModule) },
      { path: "vehicleInfo", outlet: "sidebar", loadChildren: () => import(`./vehicle-info-management/vehicle-info-management.module`).then(m => m.VehicleInfoManagementModule) },
      { path: "help", outlet: "sidebar", loadChildren: () => import(`./help-management/help-management.module`).then(m => m.HelpManagementModule) },
      { path: "privacypolicy", outlet: "sidebar", loadChildren: () => import(`./privacy-management/privacy-management.module`).then(m => m.PrivacyManagementModule) },
      { path: "termscondition", outlet: "sidebar", loadChildren: () => import(`./termscondition-management/termscondition-management.module`).then(m => m.TermsconditionManagementModule) },
      { path: "location", outlet: "sidebar", loadChildren: () => import(`./location-management/location-management.module`).then(m => m.LocationManagementModule) },
      { path: "booking", outlet: "sidebar", loadChildren: () => import(`./booking-management/booking-management.module`).then(m => m.BookingManagementModule) },
      { path: "fare", outlet: "sidebar", loadChildren: () => import(`./fare-management/fare-management.module`).then(m => m.FareManagementModule) },
      { path: "notification", outlet: "sidebar", loadChildren: () => import(`./notification-management/notification-management.module`).then(m => m.NotificationManagementModule) },
      { path: "rewards", outlet: "sidebar", loadChildren: () => import(`./rewards-management/rewards-management.module`).then(m => m.RewardsManagementModule) },
      { path: "contactus", outlet: "sidebar", loadChildren: () => import(`./contactus-management/contactus-management.module`).then(m => m.ContactusManagementModule) },
      { path: "promocode", outlet: "sidebar", loadChildren: () => import(`./promocode-management/promocode-management.module`).then(m => m.PromocodeManagementModule) },
      { path: "co2", outlet: "sidebar", loadChildren: () => import(`./co2-management/co2-management.module`).then(m => m.Co2ManagementModule) },
      { path: "pushNotification", outlet: "sidebar", loadChildren: () => import(`./push-notification-management/push-notification-management.module`).then(m => m.PushNotificationManagementModule) },
      { path: "dashboard", outlet: "sidebar", loadChildren: () => import(`./main-dashboard/main-dashboard.module`).then(m => m.MainDashboardModule) },
      { path: "payment", outlet: "sidebar", loadChildren: () => import(`./payment-management/payment-management.module`).then(m => m.PaymentManagementModule) },
      { path: "rewardsbill", outlet: "sidebar", loadChildren: () => import(`./rewards-bill-management/rewards-bill-management.module`).then(m => m.RewardsBillManagementModule) }

    ]
  },
];

@NgModule({

  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  // providers :[{ provide: LocationStrategy, useClass: HashLocationStrategy } ],
})
export class AppRoutingModule { }
