import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notification/notification.component';
import { NotificationMangementRoutingModule } from './notificationManagement-routing.module';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { AddNotificationComponent } from './add-notification/add-notification.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';



@NgModule({
  declarations: [NotificationComponent, AddNotificationComponent],
  imports: [
    CommonModule,
    NotificationMangementRoutingModule,
    UiSwitchModule,
    FormsModule ,
    ReactiveFormsModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
  ]
})
export class NotificationManagementModule { }
