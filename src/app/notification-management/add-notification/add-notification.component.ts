import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { NotificationManagementService } from 'src/app/Services/notificationManagement/notification-management.service';

@Component({
  selector: 'app-add-notification',
  templateUrl: './add-notification.component.html',
  styleUrls: ['./add-notification.component.css']
})
export class AddNotificationComponent implements OnInit {
  addNotificationForm: FormGroup = new FormGroup({})
  constructor(private fb: FormBuilder, private notifService: NotificationManagementService) { 
    this.addNotificationForm = this.fb.group({
      moduleid: new FormControl(''),
      name: new FormControl(''),
      description: new FormControl(''),
      status: new FormControl(''),
     })
  }

  ngOnInit() {
  }
  onSubmit(){
    console.log(this.addNotificationForm.value);
    this.notifService.createNotif(this.addNotificationForm.value).subscribe((data)=>{
      console.log(data);
      this.addNotificationForm.reset();
    },(err=>{
      console.log(err);
    }))
  }
}
