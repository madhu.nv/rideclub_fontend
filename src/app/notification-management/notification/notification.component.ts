import { Component, OnInit } from '@angular/core';
import { NotificationManagementService } from 'src/app/Services/notificationManagement/notification-management.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
notifications = [];
popups = [];
p=1;
searchText;
updateNotificationForm: FormGroup = new FormGroup({})
updatePopUpForm: FormGroup = new FormGroup({})
  constructor( private notifService: NotificationManagementService, private fb: FormBuilder) { 
    this.updateNotificationForm = this.fb.group({
      moduleid: new FormControl(''),
      name: new FormControl(''),
      description: new FormControl(''),
      status: new FormControl(''),
      id: new FormControl('')
     })
     this.updatePopUpForm = this.fb.group({
      moduleid: new FormControl(''),
      name: new FormControl(''),
      description: new FormControl(''),
      status: new FormControl(''),
      id: new FormControl('')
     })
  }

  ngOnInit() {
    $(document).ready(function () {
      $('#submitModal').click(function () {
        $('#updateModal').modal('toggle');
      });
    });
    $(document).ready(function () {
      $('#submitpopupModal').click(function () {
        $('#updatePopUpModal').modal('toggle');
      });
    });
    this.fetchNotifData()
    this.fetchPopUpData()
  }
    fetchNotifData(){
      this.notifService.fetchNotif().subscribe((data: any)=>{
        console.log(data);
        this.notifications = data.response
      },(err=>{
        console.log(err);
      }))
    }



    openUpdateModal(notif){
      $('#updateModal').modal('toggle');
      this.updateNotificationForm.patchValue({
        moduleid: notif.moduleid,
        name: notif.name,
        description: notif.description,
        status: notif.status,
        id: notif._id
      })
    }

    openPopModal(popup){
      $('#updatePopUpModal').modal('toggle');
      this.updatePopUpForm.patchValue({
        moduleid: popup.moduleid,
        name: popup.name,
        description: popup.description,
        status: popup.status,
        id: popup._id
      })
    }

    onSubmitUpdateData(){
      console.log(this.updateNotificationForm.value)
      this.notifService.updateNotif(this.updateNotificationForm.value).subscribe((data)=>{
        console.log(data);
        this.updateNotificationForm.reset();
        this.fetchNotifData()
      },(err=>{
        console.log(err);
      }))
    }
    fetchPopUpData(){
      this.notifService.fetchPopUp().subscribe((data: any)=>{
        console.log(data);
        this.popups = data.response
      },(err=>{
        console.log(err);
      }))
    }
    onSubmitPopupData(){
      this.notifService.updatePopUp(this.updatePopUpForm.value).subscribe((data)=>{
        console.log(data);
        this.updatePopUpForm.reset();
        this.fetchPopUpData()
      },(err=>{
        console.log(err);
      }))
    }
}
