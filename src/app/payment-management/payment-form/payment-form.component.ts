import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { PaymentManagementService } from 'src/app/Services/paymentManagement/payment-management.service';

declare var Razorpay: any;

@Component({
  selector: 'app-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrls: ['./payment-form.component.css']
})
export class PaymentFormComponent implements OnInit {
  razorpayForm: FormGroup = new FormGroup({});

  constructor(private fb: FormBuilder, private dataService: PaymentManagementService) {
    this.razorpayForm = this.fb.group({
      name: new FormControl(''),
      amount: new FormControl(''),
      email: new FormControl('')
    })
  }

  ngOnInit() {
  }

  razorPayOptions = {
    "key": "rzp_test_LwjVSUlDf0Mq44",
    "amount": 100,
    "currency": "INR",
    "name": "Rider Club",
    "description": "Admin Payment",
    "image": "https://example.com/your_logo",
    "handler": (res) => {
      console.log(res)
    },
    "theme": {
      "color": "green"
    }
  }

 razorPayData = {
  
    "account_number": "2323230062563730",   
 "amount": 100, 
    "currency": "INR",   
     "mode": "NEFT",   
      "purpose": "refund",  
        "fund_account": {"account_type": "bank_account",
        "bank_account": {"name": "Noor jabeena", 
        "ifsc": "SBIN0040109",
        "account_number": "32323097690"
        },
        "contact": {
            "name": "Noor jabeena", 
            "email": "noorjabeena1@gmail.com", 
            "contact": "8105376872",
            "type": "employee"
            }
            },    
            "queue_if_low_balance": false,
            "reference_id": "Acme Transaction ID 12345",
            "narration": "Acme Corp Fund Transfer",
            "notes": {
                "notes_key_1": "Beam me up Scotty",
                "notes_key_2": "Engage"    }
                
}

  buyRazorPay(formData: any) {
    console.log(this.razorPayData, "rrrrrr")

    this.dataService.razorPayOrder(this.razorPayData).subscribe((res) => {
      console.log(res, "response")
    });
    this.razorPayOptions.handler = this.razorpayResponseHandler
    var rzpl = new Razorpay(this.razorPayOptions)
    rzpl.open()
    console.log("opened")
  }

  razorpayResponseHandler(response) {
    console.log(response, "nnnnn")
  }
}
