import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentFormComponent } from './payment-form/payment-form.component';
import { PaymentMangementRoutingModule } from './paymentManagement-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [PaymentFormComponent],
  imports: [
    CommonModule,
    PaymentMangementRoutingModule,
    FormsModule ,
    ReactiveFormsModule,
  ]
})
export class PaymentManagementModule { }
