import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBikeCo2NormsComponent } from './add-bike-co2-norms.component';

describe('AddBikeCo2NormsComponent', () => {
  let component: AddBikeCo2NormsComponent;
  let fixture: ComponentFixture<AddBikeCo2NormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBikeCo2NormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBikeCo2NormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
