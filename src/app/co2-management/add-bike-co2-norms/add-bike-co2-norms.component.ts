import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Co2ManagementService } from 'src/app/Services/co2Management/co2-management.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-bike-co2-norms',
  templateUrl: './add-bike-co2-norms.component.html',
  styleUrls: ['./add-bike-co2-norms.component.css']
})
export class AddBikeCo2NormsComponent implements OnInit {
  bikeNormForm: FormGroup = new FormGroup({});
  numPattern = "^[0-9]*$";
  submitted = false;
  todayDate;
  constructor( private co2Service: Co2ManagementService, 
    private fb: FormBuilder,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router) { 

    this.bikeNormForm = this.fb.group({
      petrol_norms: new FormControl('',  [Validators.required, Validators.pattern(this.numPattern)]),
      diesel_norms: new FormControl('',  [Validators.required, Validators.pattern(this.numPattern)]),
      // co2grams: new FormControl('',  [Validators.required, Validators.pattern(this.numPattern)]),
      date: new FormControl('')
     })
  }

  ngOnInit() {
    var datePipe=new DatePipe("en-US");
    this.todayDate=datePipe.transform(new Date(), 'yyyy-MM-dd');
  }

  onSubmit() {
    this.submitted = true;
    this.co2Service.craeteBikeNorms(this.bikeNormForm.value).subscribe((data)=>{
      console.log(data);
      this.bikeNormForm.reset();
      this.router.navigate(['/side-menu',{outlets:{sidebar:'co2'}}], {relativeTo: this.route})
    },(err=>{
      console.log(err);
      this.toastr.error(`${err.error.message}`)
    }))
  }
  get f() { return this.bikeNormForm.controls; }
}
