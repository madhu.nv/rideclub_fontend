import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCarCo2NormsComponent } from './add-car-co2-norms.component';

describe('AddCarCo2NormsComponent', () => {
  let component: AddCarCo2NormsComponent;
  let fixture: ComponentFixture<AddCarCo2NormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCarCo2NormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCarCo2NormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
