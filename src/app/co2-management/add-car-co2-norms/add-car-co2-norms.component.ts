import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { Co2ManagementService } from 'src/app/Services/co2Management/co2-management.service';

@Component({
  selector: 'app-add-car-co2-norms',
  templateUrl: './add-car-co2-norms.component.html',
  styleUrls: ['./add-car-co2-norms.component.css']
})
export class AddCarCo2NormsComponent implements OnInit {
  carNormForm: FormGroup = new FormGroup({})
  submitted = false;
  numPattern = "^[0-9]*$";
  todayDate;
  constructor( private co2Service: Co2ManagementService, 
    private fb: FormBuilder,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router) { 
    this.carNormForm = this.fb.group({
      petrol_norms: new FormControl('', [Validators.required]),
      diesel_norms: new FormControl('', [Validators.required]),
      // co2grams: new FormControl('',  [Validators.required]),
      date: new FormControl('', Validators.required)
     })
  }

  ngOnInit() {
    var datePipe=new DatePipe("en-US");
    this.todayDate=datePipe.transform(new Date(), 'yyyy-MM-dd');
  }

  onSubmit() {
    this.submitted = true;
    this.co2Service.createCarNorms(this.carNormForm.value).subscribe((data)=>{
      this.carNormForm.reset() ;
      this.router.navigate(['/side-menu',{outlets:{sidebar:'co2'}}], {relativeTo: this.route})  
    },(err=>{
      console.log(err);
      this.toastr.error(`${err.error.message}`)
    }))
  }

  get f() { return this.carNormForm.controls; }

}
