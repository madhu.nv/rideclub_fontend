import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Co2MangementRoutingModule } from './co2Management-routing.module';
import { Co2Component } from './co2/co2.component';
import { AddBikeCo2NormsComponent } from './add-bike-co2-norms/add-bike-co2-norms.component';
import { AddCarCo2NormsComponent } from './add-car-co2-norms/add-car-co2-norms.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';



@NgModule({
  declarations: [Co2Component, AddBikeCo2NormsComponent, AddCarCo2NormsComponent],
  imports: [
    CommonModule,
    Co2MangementRoutingModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    FormsModule ,
    ReactiveFormsModule,
  ]
})
export class Co2ManagementModule { }
