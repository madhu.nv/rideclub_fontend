import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddBikeCo2NormsComponent } from './add-bike-co2-norms/add-bike-co2-norms.component';
import { AddCarCo2NormsComponent } from './add-car-co2-norms/add-car-co2-norms.component';
import { Co2Component } from './co2/co2.component';

const routes: Routes = [
    { path: '', component: Co2Component},
    { path: 'add_bike_norms', component: AddBikeCo2NormsComponent},
    { path: 'add_car_norms', component: AddCarCo2NormsComponent}
    
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Co2MangementRoutingModule {}
