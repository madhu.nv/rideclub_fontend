import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Co2ManagementService } from 'src/app/Services/co2Management/co2-management.service';
import { DatePipe } from '@angular/common';
import { ExcelServicesService } from 'src/app/Services/excel/excel-services.service';
declare var $: any;

@Component({
  selector: 'app-co2',
  templateUrl: './co2.component.html',
  styleUrls: ['./co2.component.css']
})
export class Co2Component implements OnInit {
  carNormsArray = [];
  bikeNormsArray = [];
  id;
  date;
  bdate;
  searchText;
  p = 1;
  eDate;
  numPattern = "^[0-9]*$";
  todayDate;
  submitted = false;
  submitData = false;
  excel = []
  obj = {}
  carNormUpdateForm: FormGroup = new FormGroup({})
  BikeNormUpdateForm: FormGroup = new FormGroup({})
  constructor(private co2Service: Co2ManagementService,
    private fb: FormBuilder,
    private excelService: ExcelServicesService) {
    this.carNormUpdateForm = this.fb.group({
      petrol_norms: new FormControl('', [Validators.required]),
      diesel_norms: new FormControl('', [Validators.required]),
      date: new FormControl(''),
      id: new FormControl('')
    })

    this.BikeNormUpdateForm = this.fb.group({
      petrol_norms: new FormControl('', [Validators.required]),
      diesel_norms: new FormControl('', [Validators.required]),
      date: new FormControl(''),
      id: new FormControl('')
    })
  }

  ngOnInit() {
    $(document).ready(function () {
      $('#updateCarSubmit').click(function () {
        $('#carNormsModal').modal('toggle');
      });
    });
    $(document).ready(function () {
      $('#updateBikeSubmit').click(function () {
        $('#bikeModal').modal('toggle');
      });
    });

    var datePipe = new DatePipe("en-US");
    this.todayDate = datePipe.transform(new Date(), 'yyyy-MM-dd');

    this.getCarNorms();
    this.getBikeNorms();

  }

  getCarNorms() {
    this.co2Service.fetchCarNorms().subscribe((data: any) => {
      console.log(data.response);
      this.carNormsArray = data.response
    }, (err => {
      console.log(err);
    }))
  }

  updateCarNormsSubmit(carNorm) {
    $('#carNormsModal').modal('toggle');
    var datePipe = new DatePipe("en-US");
    this.date = datePipe.transform(carNorm.date, 'yyyy-MM-dd');
    this.carNormUpdateForm.patchValue({
      id: carNorm._id,
      petrol_norms: carNorm.petrol_norms,
      diesel_norms: carNorm.diesel_norms,
      date: this.date
    })
  }

  onSubmit() {
    this.submitted = true;
    this.co2Service.updateCarNorms(this.carNormUpdateForm.value).subscribe((data) => {
      console.log(data);
      this.getCarNorms();
    }, (err => {
      console.log(err);
    }))
  }

  get f() { return this.carNormUpdateForm.controls; }

  exportAsXLSX() {
    
    this.carNormsArray.forEach(element => {
      var datePipe = new DatePipe("en-US");
    this.eDate = datePipe.transform(element.date, 'yyyy-MM-dd');
      this.obj = {
        petrol_norms: element.petrol_norms,
        diesel_norms: element.diesel_norms,
        date: this.eDate
      }
      this.excel.push(this.obj)

    });
    this.excelService.exportAsExcelFile(this.excel, 'Car Norms');
  }


  deleteCarNorm(id) {
    var r = confirm("Do you really want to delete this record?");
    if (r == true) {
      this.co2Service.deleteCarNorms(id).subscribe((data) => {
        console.log(data);
        this.getCarNorms()
      }, (err => {
        console.log(err);
      }))
    } else {
      console.log("Some problem")
    }

  }

  // Bike Norms

  getBikeNorms() {
    this.co2Service.fetchBikeNorms().subscribe((data: any) => {
      console.log(data.response);
      this.bikeNormsArray = data.response
    }, (err => {
      console.log(err);
    }))
  }

  updateBikeNorm(bikeNorm) {
    $('#bikeModal').modal('toggle');
    var datePipe = new DatePipe("en-US");
    this.bdate = datePipe.transform(bikeNorm.date, 'yyyy-MM-dd');

    this.BikeNormUpdateForm.patchValue({
      id: bikeNorm._id,
      petrol_norms: bikeNorm.petrol_norms,
      diesel_norms: bikeNorm.diesel_norms,
      date: this.bdate
    })
  }

  onSubmitBikeNorm(data) {
    this.submitData = true;
    this.co2Service.updateBikeNorms(this.BikeNormUpdateForm.value).subscribe((data) => {
      console.log(data);
      this.getBikeNorms();
    }, (err => {
      console.log(err);
    }))
  }

  get n() { return this.BikeNormUpdateForm.controls; }

  deleteBikeNorm(id) {
    var r = confirm("Do you really want to delete this record?");
    if (r == true) {
      this.co2Service.deleteBikeNorms(id).subscribe((data) => {
        console.log(data);
        this.getBikeNorms()
      }, (err => {
        console.log(err);
      }))
    } else {
      console.log("Some problem")
    }
  }
}
