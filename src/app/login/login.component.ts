import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../Services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  loginform: FormGroup = new FormGroup({});
  constructor( private authService: AuthService, private fb: FormBuilder, private toastr: ToastrService, private router: Router) { 
    this.loginform = fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }
  get f(){
    return this.loginform.controls
  }
  // submit(){
  //   console.log(this.loginform.value);
  //   this.authService.SignIn(this.loginform.value.userName,this.loginform.value.userPassword)
  //   this.toastr.success(`Login Successfully ${this.loginform.value.userName}`)
  // }
  signIn() {
    this.authService.SignIn(this.email, this.password).then(res=>{
      console.log(res, "kkkkk")
      this.email = '';
      this.password = '';
     
      this.toastr.success('LoggedIn successfully') 
      this.router.navigate(['/side-menu'])
    })  .catch(err => {
      console.log('Something is wrong:',err.message);
      window.alert(err)
    });
   
    }

      signIn1() {
      this.authService.SignIn1(this.email, this.password)
      .subscribe(res=>{
        console.log(res)
        this.toastr.success('LoggedIn successfully') 
        this.router.navigate(['/side-menu'])
      })
  }
  ngOnInit() {
  }

}
