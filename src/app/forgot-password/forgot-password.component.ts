import { Component, OnInit } from '@angular/core';
import { AuthService } from '../Services/auth/auth.service';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  
  constructor(  public authService: AuthService) { }

  ngOnInit() {
  }
  forgotPassword(email){
    this.authService.ForgotPassword(email).then(res=>{
      window.alert('Password reset email sent, check your inbox.');
    }).catch((error) => {
      window.alert(error)
    })
  }
}
