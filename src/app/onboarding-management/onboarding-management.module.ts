import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnboardingMangementRoutingModule } from './onboardingManagement-routing.module';
import { OnboardingScreenComponent } from './onboarding-screen/onboarding-screen.component';
import { OnboardingViewComponent } from './onboarding-view/onboarding-view.component';
import { AddOnboardingComponent } from './add-onboarding/add-onboarding.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { Ng2SearchPipeModule } from 'ng2-search-filter';



@NgModule({
  declarations: [OnboardingScreenComponent, OnboardingViewComponent,AddOnboardingComponent],
  imports: [
    CommonModule,
    OnboardingMangementRoutingModule,
    NgxPaginationModule,
    FormsModule ,
    ReactiveFormsModule,
    CKEditorModule,
    Ng2SearchPipeModule,
  ]
})
export class OnboardingManagementModule { }
