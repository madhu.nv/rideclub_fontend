import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { OnboardingService } from 'src/app/Services/onboarding/onboarding.service';


declare var $: any;

@Component({
  selector: 'app-onboarding-screen',
  templateUrl: './onboarding-screen.component.html',
  styleUrls: ['./onboarding-screen.component.css']
})
export class OnboardingScreenComponent implements OnInit {
  p=1;
  submitted = false;
  // itemsPerPage=5;
  public Editor = ClassicEditor;
  images;
  id;
  someImage;
  onboardingUpdateForm : FormGroup = new FormGroup({})
  getOnboardArray : any
  searchText;
  constructor( private fb: FormBuilder, private onboardingService : OnboardingService) { 
  this.onboardingUpdateForm = this.fb.group({
    onboarding_data: new FormControl('', [Validators.required]),
    onboarding_image: new FormControl(''),
    id : new FormControl('')
  })
  }
  get f() { return this.onboardingUpdateForm.controls; }

  ngOnInit() {
    this.getOnboarding()

    $(document).ready(function () {
      $('#submitUpdateData').click(function () {
        $('#updateOnboarding').modal('toggle');
      });
    });
   
  }
  // viewOnboarding(onboarding){
  //   console.log(onboarding, "onboatding view")
  //   console.log(onboarding._id, "id")
  // }
 getOnboarding(){
   this.onboardingService.fetchOnboarding().subscribe((onboardingData:any)=>{
     console.log(onboardingData.response)
    this.getOnboardArray = onboardingData.response;
    console.log(this.getOnboardArray)
   })
 }
updateOnboardingModal(onboarding){
 console.log(onboarding)
 
 var id = onboarding.id

 this.someImage = onboarding.onboarding_image
 console.log(this.someImage, "vhddh")
 console.log(id, "userId")
  $('#updateOnboarding').modal('toggle')
  this.onboardingUpdateForm .patchValue({
    onboarding_data: onboarding.onboarding_data,
    onboarding_image: this.someImage,
    id: onboarding._id,
   })
}

handleEvent(event){
  if(event.target.files.length > 0){
    const file = event.target.files[0];
    this.images = file
  }
}

submitUpdatedForm(onboardingUpdateForm){
  this.submitted = true;

  // debugger
 console.log(onboardingUpdateForm.value, "onboarding update form")
 console.log(onboardingUpdateForm.value.id, "id")
  const formData = new FormData();
  formData.append('onboarding_image',this.images);
    formData.append("onboarding_data", onboardingUpdateForm.value.onboarding_data);
    formData.append('id', onboardingUpdateForm.value.id);
   this.onboardingService.updateOnboarding(formData).subscribe((data)=>{
   console.log("Data",data)
    this.onboardingUpdateForm.reset()
    this.getOnboarding()
  })
}


delete(onboarding){
  console.log(onboarding, "Deleting data")
 var r = confirm("Do you really want to delete this record?");
 if(r==true){
  this.onboardingService.deleteOnboarding(onboarding).subscribe((data)=>{
    console.log(data, "onboarding delete data")
    this.getOnboarding()
  },err=>{
    console.log(err);
  },()=>{
    console.log("Deleted successfully");
  })
 }else{
   console.log("cancelled")
 }
 
}




}
