import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OnboardingService } from 'src/app/Services/onboarding/onboarding.service';


@Component({
  selector: 'app-onboarding-view',
  templateUrl: './onboarding-view.component.html',
  styleUrls: ['./onboarding-view.component.css']
})
export class OnboardingViewComponent implements OnInit {
onboardingArray: any
onboarding_id: any;
onboardingData : any;
  constructor( private onboardingService: OnboardingService, private actRoute: ActivatedRoute) { 
    
    this.onboarding_id = this.actRoute.snapshot.params._id;
    console.log(this.onboarding_id, "dfhdh")
    this.fetchOnboardingData(this.onboarding_id);       
  }

  ngOnInit() {
  }
fetchOnboardingData(onboarding_id){
  console.log(onboarding_id, "abcd")
  this.onboardingService.fetchOnboardingById(onboarding_id).subscribe((data2:any)=>{
   this.onboardingArray = data2.response.onboarding_image;
   console.log(this.onboardingArray)
   this.onboardingData = data2.response.onboarding_data;
    console.log(this.onboardingData, "onboardingArray")
    
  })
}


}
