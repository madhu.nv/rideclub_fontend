 import { Component, OnInit } from '@angular/core';
 import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
 import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { OnboardingService } from 'src/app/Services/onboarding/onboarding.service';


 declare var $: any;

 @Component({
   selector: 'app-add-onboarding',
   templateUrl: './add-onboarding.component.html',
   styleUrls: ['./add-onboarding.component.css']
 })
 export class AddOnboardingComponent implements OnInit {
   images;
   editorConfig = {
     placeholder: 'Type the content here!',
   };
   public Editor = ClassicEditor;myFiles;url;
   onBoardingForm : FormGroup =new FormGroup({})
   createArray: any
   preview: string;
   constructor( private fb: FormBuilder, private onboardingService: OnboardingService) { 
     this.onBoardingForm = this.fb.group({
       onboarding_data: new FormControl('', [Validators.required]),
     
     })
   }
   get f() {
     return this.onBoardingForm.controls
   }
   ngOnInit() {
   
   }
   selectImage(event){
     if(event.target.files.length > 0){
       const file = event.target.files[0];
       this.images = file
     }
   }
   onboardingSubmit(onBoardingForm){
    const formData = new FormData();
    formData.append('onboarding_image',this.images);
    formData.append("onboarding_data", onBoardingForm.value.onboarding_data);
    this.onboardingService.createOnboarding(formData).subscribe((data)=>{
      console.log(data)
      this.onBoardingForm.reset()
      
    })

   }


 }



