import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OnboardingScreenComponent } from './onboarding-screen/onboarding-screen.component';
import { AddOnboardingComponent } from './add-onboarding/add-onboarding.component';
import { OnboardingViewComponent } from './onboarding-view/onboarding-view.component';

const routes: Routes = [
    { path: '', component: OnboardingScreenComponent},
    { path: 'add_onboarding', component: AddOnboardingComponent},
    { path: 'view_onboarding/:_id', component: OnboardingViewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnboardingMangementRoutingModule {}
