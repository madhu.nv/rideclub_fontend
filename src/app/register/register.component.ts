import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { confirmedValidator} from '../confirmed.validator'
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../Services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email: string;
  password: string;
  form: FormGroup = new FormGroup({});
  constructor( private authService: AuthService, private fb: FormBuilder, private toastr: ToastrService, private router: Router) {
  
    this.form = fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      cuserPwd : ['', [Validators.required]]
    }, 
    {
      validators: confirmedValidator('password', 'cuserPwd')
    })
  }
  get f(){
    return this.form.controls;
  }

  // signUp(){
  //   alert(this.email)
  //   this.authService.SignUp(this.email, this.password).then(res=>{
  //     console.log('Successfully signed up!', res);
  //     this.email = '';
  //     this.password = '';
  //     this.toastr.success('Registered successfully') 
  //     this.router.navigate(['/login'])
  //     this.authService.SendVerificationMail()
  //   }).catch(error=>{
  //     console.log(error)
  //     console.log('Something is wrong:', error.message);
  //       window.alert(error)
  //   })

  signUp(){
    alert(this.email)
    this.authService.SignUp(this.email, this.password).subscribe(res=>{
      console.log('Successfully signed up!', res);
      this.email = '';
      this.password = '';
      this.toastr.success('Registered successfully') 
      this.router.navigate(['/login'])
      this.authService.SendVerificationMail()
    })
   
  // console.log(this.form.value)
  //   this.authService.SignUp(this.form.value.userEmail,this.form.value.userPwd)
   
  }
  signIn() {
    this.authService.SignIn(this.email, this.password);
    this.email = '';
    this.password = '';
    }
     
    signOut() {
    this.authService.SignOut();
    }

  ngOnInit() { 
  }

}
