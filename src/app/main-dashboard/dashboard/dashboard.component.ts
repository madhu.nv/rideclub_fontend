import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js'
import { DasboardService } from 'src/app/Services/DashboardManagement/dasboard.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  doughnut: any;
  chart;
  riders;
  travellers;
  rides;
  currentRides;
  upcomingRides;
  verifiedTaveller;
  verifiedRider;
  constructor( private dashboardService : DasboardService) { }



  ngOnInit() {
    // this.fetchDashboardData();

    this.dashboardService.fetchData().subscribe((data: any)=>{
      console.log(data);
      this.riders = data.response.riders
      this.travellers = data.response.travellers
      this.rides = data.response.rides
      this.upcomingRides = data.response.scheduleRides
      this.verifiedTaveller = data.response.verifiedTraveller
      this.verifiedRider = data.response.verifiedRider
      this.doughnut =  new Chart('doughnut',{
        type: 'doughnut',
        options: {
          responsive: true,
          title: {
            display: true,
            text: 'Verified riders and travellers'
          },legend: {
            position: 'top',
          },animation: {
            animateScale: true,
            animateRotate: true
          }
        },
        data: {
          datasets: [{
            data: [this.verifiedTaveller, this.verifiedRider],
            backgroundColor: [ "#5cb85c", "#f0ad4e"],
            label: 'Dataset 1'
          }],
          labels: ['VerifiedTravellers','VerifiedRiders']
        }
      })
    },(err=>{
      console.log(err)
    }))


  }

 
  // fetchDashboardData(){
  //   this.dashboardService.fetchData().subscribe((data: any)=>{
  //     console.log(data);
  //     this.riders = data.response.riders
  //     this.travellers = data.response.travellers
  //     this.rides = data.response.rides
  //     this.upcomingRides = data.response.scheduleRides
  //     this.verifiedTaveller = data.response.verifiedTraveller
  //     this.verifiedRider = data.response.verifiedRider
  //   },(err=>{
  //     console.log(err)
  //   }))
  // }

}
