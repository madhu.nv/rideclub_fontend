import { Component, OnInit, NO_ERRORS_SCHEMA } from '@angular/core';
import { TravellermanagementService } from 'src/app/Services/travellerManagement/travellermanagement.service';
import { ExcelServicesService } from 'src/app/Services/excel/excel-services.service';

@Component({
  selector: 'app-traveller',
  templateUrl: './traveller.component.html',
  styleUrls: ['./traveller.component.css']
})
export class TravellerComponent implements OnInit {
  page:number = 1;
  collection = [];
  travellerdetail = true;
  getTravellerArray = [];
  searchText;
  excel = [];
  img;
  abc;
  constructor(private service: TravellermanagementService, private excelService:ExcelServicesService) { 
    for (let i = 1; i <= 100; i++) {
      this.collection.push(`traveller ${i}`);
    }
   
  }

  ngOnInit() {
    this.getTravellerData()
   
  }

  getTravellerData() {
   this.service.getTraveller().subscribe((data: any) => {
      this.getTravellerArray = data.response;
      console.log(this.getTravellerArray)
    })
 
  }
  getUrl(filename){
   
    let image = "http://rider-club.dxminds.online/rider_club_backend/Public/user/profilepic/"+ filename;
    return image;
  }
 

  exportAsXLSX():void {  
  this.getTravellerArray.forEach(element => {
    this.excel.push(element)
  
  });
 
    this.excelService.exportAsExcelFile(this.excel, 'Traveller');  
 }

  delete(traveller) {
    var r = confirm("Do you really want to delete this record?")
    if(r==true){
      console.log(traveller)
      this.service.deleteTraveller(traveller).subscribe(deletData=>{
        console.log(deletData)
        this.getTravellerData()
      })
    }else{
      console.log("cancelled")

    }
   
    
  }

}
