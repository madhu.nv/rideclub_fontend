import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TravellermanagementService } from 'src/app/Services/travellerManagement/travellermanagement.service';

@Component({
  selector: 'app-traveller-kyc-document',
  templateUrl: './traveller-kyc-document.component.html',
  styleUrls: ['./traveller-kyc-document.component.css']
})
export class TravellerKycDocumentComponent implements OnInit {
  docId_id : String
  getKycArray = []
  govtID : any;
  imageData: any;
  approvalForm: FormGroup = new FormGroup({})
  rejectForm: FormGroup = new FormGroup({})
  govtIdVerifStatus
  constructor(private actRoute: ActivatedRoute, private service: TravellermanagementService, private fb: FormBuilder, private toastr: ToastrService) { 

    this.docId_id = this.actRoute.snapshot.params._id
    console.log(this.docId_id, "docID");

    this.approvalForm = this.fb.group({
      govtIdVerifStatus: new FormControl(1),
      type: new FormControl('govtid'),
      uid :new FormControl(`${this.docId_id}`)
    })

    //Reject status
    this.rejectForm = this.fb.group({
      govtIdVerifStatus: new FormControl(0),
      type: new FormControl('govtid'),
      uid :new FormControl(`${this.docId_id}`)
    })

  }

  ngOnInit() {
    this.getKycTraveller(this.docId_id)
    this.getTravellerSingleData()
  }
  getKycTraveller(docId_id){

    this.service.getKyc(docId_id).subscribe((kycData:any)=>{
      console.log(kycData,"kycdata")
    var data = kycData.data
    this.imageData = data.govtProof.govtIdDoc
      this.govtID = data.govtProof.govtId
     console.log(this.govtID)
    })
  }

  approvalStatus(approvalForm){
    console.log(approvalForm.value, "dhfjhj")
  var govtIdVerifStatus = approvalForm.value.govtIdVerifStatus;
   var type = approvalForm.value.type;
   var uid = approvalForm.value.uid;
   console.log(uid, "mmm")
    this.service.postApproval({ "govtIdVerifStatus": govtIdVerifStatus,"type": type,"uid":uid }).subscribe(approvalData=>{
      console.log(approvalData, "approvaldta")
      this.toastr.success('Approved');
    })
  }

  rejectStatus(rejectForm){
    console.log(rejectForm.value)
    var govtIdVerifStatus = rejectForm.value.govtIdVerifStatus;
     var type = rejectForm.value.type;
     var uid = rejectForm.value.uid;
      this.service.postReject({ "govtIdVerifStatus": govtIdVerifStatus,"type": type,"uid":uid }).subscribe(rejectData=>{
        console.log(rejectData, "rejectData")
        this.toastr.error( 'Rejected');
      })
    }
        getTravellerSingleData(){
          this.service.getSingleTraveller(this.docId_id).subscribe((data: any)=>{
            console.log(data, "sinle status");
            this.govtIdVerifStatus = data.response.govtIdVerifStatus
            console.log(this.govtIdVerifStatus)
            this.getKycTraveller(this.docId_id)
            this.getTravellerSingleData()
          },(err=>{
            console.log(err) ;
          }))
        }

}
