import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravellerKycDocumentComponent } from './traveller-kyc-document.component';

describe('TravellerKycDocumentComponent', () => {
  let component: TravellerKycDocumentComponent;
  let fixture: ComponentFixture<TravellerKycDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TravellerKycDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TravellerKycDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
