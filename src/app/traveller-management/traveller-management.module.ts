import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TravellerMangementRoutingModule } from './travellermangement-routing.module';
import { TravellerComponent } from './traveller/traveller.component';
import { TravellerKycDocumentComponent } from './traveller-kyc-document/traveller-kyc-document.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [TravellerComponent, TravellerKycDocumentComponent],
  imports: [
    CommonModule,
    TravellerMangementRoutingModule,
    NgxPaginationModule,
    UiSwitchModule,
    FormsModule ,
    ReactiveFormsModule ,
    Ng2SearchPipeModule 
  ]
})
export class TravellerManagementModule { }
