import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TravellerComponent } from './traveller/traveller.component';
import { TravellerKycDocumentComponent } from './traveller-kyc-document/traveller-kyc-document.component';




const routes: Routes = [
    { path: '', component: TravellerComponent},
    { path: 'traveller_kyc/:_id', component: TravellerKycDocumentComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravellerMangementRoutingModule { }
