import { Component, OnInit } from '@angular/core';
import { AboutusService } from 'src/app/Services/aboutUs/aboutus.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
aboutUsArray:any = []
ckeConfig: any;  
  public Editor = ClassicEditor;
 
  constructor( private service:AboutusService) { 
   
  }

  ngOnInit() {
    this.getDescription()
    
  }
    getDescription(){
      this.service.getData().subscribe((data:any)=>{
      console.log(data)
        this.aboutUsArray=data.response[0].description;
        console.log(this.aboutUsArray, "aboutus Array")
      })
    }
}
