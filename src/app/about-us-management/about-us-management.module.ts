import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutUsMangementRoutingModule } from './aboutusmangement-routing.module';
import { AboutusComponent } from './aboutus/aboutus.component';
import { AboutUsAddComponent } from './about-us-add/about-us-add.component';
import { ViewComponent } from './view/view.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { EditAboutusComponent } from './edit-aboutus/edit-aboutus.component';



@NgModule({
  declarations: [AboutusComponent, AboutUsAddComponent, ViewComponent, EditAboutusComponent],
  imports: [
    CommonModule,
    AboutUsMangementRoutingModule,
    NgxPaginationModule,
    FormsModule ,
    ReactiveFormsModule,
    CKEditorModule,
  ]
})
export class AboutUsManagementModule { }
