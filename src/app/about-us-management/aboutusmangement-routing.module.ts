import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutusComponent } from './aboutus/aboutus.component';
import { AboutUsAddComponent } from './about-us-add/about-us-add.component';
import { ViewComponent } from './view/view.component';
import { EditAboutusComponent } from './edit-aboutus/edit-aboutus.component';

const routes: Routes = [
    { path: '', component: AboutusComponent},
    { path: 'view_aboutus', component: ViewComponent},
    { path: 'edit_aboutus/:id', component: EditAboutusComponent},
    { path: 'add_aboutus', component: AboutUsAddComponent},
    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutUsMangementRoutingModule { }
