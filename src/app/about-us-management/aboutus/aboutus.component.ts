import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AboutusService } from 'src/app/Services/aboutUs/aboutus.service';



declare var $: any;

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']
})
export class AboutusComponent implements OnInit {
  ckeConfig: any;
  public Editor = ClassicEditor;
  ckEditorForm: FormGroup = new FormGroup({})
  aboutUs: any = []
  editDataArray = []
  contentArray: any
  data_id:any;
  _id:any
  constructor(private router: Router,private route: ActivatedRoute, private fb: FormBuilder, private aboutusService: AboutusService) {
    this.ckEditorForm = this.fb.group({
      description: new FormControl('', [Validators.required]),
      id: new FormControl('', [Validators.required])
    })
  }

  ngOnInit() {
    $(document).ready(function () {
      $('#submitUpdateData').click(function () {
        $('#myModal').modal('toggle');
      });
    });

    this.getContent()
  }
  get f() {
    return this.ckEditorForm.controls
  }
  getContent() {
    this.aboutusService.getData().subscribe((data: any) => {
      console.log(data,"getdataa")
      this.contentArray = data.response[0].description;
      this._id = data.response[0]._id
     
    })
  }
  updationDation() {
    // var data_id = this._id
    // console.log(data_id, "id")
    // $('#myModal').modal('toggle');
    // this.ckEditorForm.patchValue({
    //   description: this.contentArray,
    //   id: this._id
    // })
    var id = this._id
    console.log(this._id)
    this.router.navigate(['./edit_aboutus',this._id], {relativeTo: this.route})
    // this.router.navigate(['edit_aboutus',id], {relativeTo: this.route})
  }

  Submit(ckEditorForm){
    var description = ckEditorForm.value.description
  
    console.log(description, "fvhfjhbf")
    this.aboutusService.updateAboutUs({"description":ckEditorForm.value.description}).subscribe((updateAboutUs)=>{
      console.log(updateAboutUs)
      this.ckEditorForm.reset()
      this.getContent()
    })
  }

  delete(){
    console.log(this._id)
    confirm("Do you really want to delete this record?");
  this.aboutusService.deleteAboutUs(this._id).subscribe((deleteData)=>{
    console.log(deleteData)
    this.getContent()
  })
  }

  // Submit(ckEditorForm) {
  //   console.log(ckEditorForm.value, "submit data")
  //   if (this.contentArray.length == 0) {
  //     console.log(this.contentArray.length, "length of array")
  //     this.aboutusService.postData(this.ckEditorForm.value).subscribe((data: any) => {
  //       this.aboutUs = data.response
  //       console.log(this.aboutUs[0].data._id, "posted data");
  //       this.getContent()
  //     })
  //   } else {
  //     console.log("managing")
  //     console.log("data is there", ckEditorForm.value)
  //     var data = {
  //       formvalue: ckEditorForm.value,
  //       id: this.contentArray[0]._id

  //     }
  //     console.log(data)
  //     this.aboutusService.updateAboutUs(data).subscribe((data) => {
  //       console.log(data, "updated data");
  //       this.ckEditorForm.reset()
  //       this.getContent()
  //     })
  //   }

  // }
}
