import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AboutusService } from 'src/app/Services/aboutUs/aboutus.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-aboutus',
  templateUrl: './edit-aboutus.component.html',
  styleUrls: ['./edit-aboutus.component.css']
})
export class EditAboutusComponent implements OnInit {
  ckeConfig: any;  
  public Editor = ClassicEditor;
  editcontentArray: any
  id: any
  submitted = false;
  editckEditorForm: FormGroup = new FormGroup({})
  constructor(private fb: FormBuilder, private aboutusService: AboutusService, private actRoute: ActivatedRoute, private router: Router) { 
    this.editckEditorForm = this.fb.group({
      description: new FormControl('', [Validators.required]),
      id: new FormControl('', [Validators.required])
    })
  }
  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "docID")
    this.getEditContent()
   
  }
  getEditContent() {
    this.aboutusService.getDataById( this.id).subscribe((getdata: any) => {
      console.log(getdata,"getdataa")
      this.editcontentArray = getdata.data.description;
      console.log(this.editcontentArray)
      this.id = getdata.data._id
     
    })
  }

  get f() { return this.editckEditorForm.controls; }
  onSubmit(editckEditorForm){
    this.submitted = true;
    console.log(editckEditorForm.value)
    var description = editckEditorForm.value.description
    console.log(description)
    this.aboutusService.updateAboutUs({"description":editckEditorForm.value.description, "id":this.id}).subscribe((updateAboutUs)=>{
      console.log(updateAboutUs)
      this.editckEditorForm.reset()
      this.router.navigate(['/side-menu',{outlets:{sidebar:'aboutus'}}], {relativeTo: this.actRoute})
    })
  }

}
