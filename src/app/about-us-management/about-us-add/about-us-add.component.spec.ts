import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutUsAddComponent } from './about-us-add.component';

describe('AboutUsAddComponent', () => {
  let component: AboutUsAddComponent;
  let fixture: ComponentFixture<AboutUsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutUsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutUsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
