import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AboutusService } from 'src/app/Services/aboutUs/aboutus.service';



@Component({
  selector: 'app-about-us-add',
  templateUrl: './about-us-add.component.html',
  styleUrls: ['./about-us-add.component.css']
})
export class AboutUsAddComponent implements OnInit {
  ckeConfig: any;  
  public Editor = ClassicEditor;
  submitted = false;
  addAboutUsForm: FormGroup = new FormGroup({})
  constructor(private fb: FormBuilder, private aboutusService: AboutusService) { 

    this.addAboutUsForm = this.fb.group({
      description: new FormControl('', [Validators.required])

    })
  }

  ngOnInit() {
    this.ckeConfig = {    
      allowedContent: false,    
      // extraPlugins: 'divarea',    
      forcePasteAsPlainText: true    
    };    
  }

  get f() { return this.addAboutUsForm.controls}

  submitAddAboutus(addAboutUsForm){
    this.submitted = true;
    console.log(addAboutUsForm.value)
    this.aboutusService.postData(addAboutUsForm.value).subscribe((data)=>{
      console.log(data, "posted data successfully")
      this.addAboutUsForm.reset()
    })
  }
}
