import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {NgxPaginationModule} from 'ngx-pagination';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
// import { AngularFireDatabaseModule } from '@angular/fire/database';
// import { AngularFireAuthModule } from '@angular/fire/auth';
// import { AngularFireModule } from '@angular/fire';
// import { MessagingService } from '';

import { AsyncPipe } from '../../node_modules/@angular/common';





import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component'; 
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { AuthenticationGuard } from './Services/auth/authentication.guard';
import { DatePipe } from '@angular/common';
import { AuthService } from './Services/auth/auth.service';
import { environment } from '../environments/environment';
import * as firebase from 'firebase';
import { AngularFireAuthGuard } from '@angular/fire/auth-guard';
import { HashLocationStrategy, LocationStrategy  } from '@angular/common';
firebase.initializeApp(environment.firebase); 
// var config = {
//   apiKey: "AIzaSyBQAQ2_b88JPvCb2S7r1_OXsl7EOubiYdk",
//   authDomain: "riderclubadmin-22474.firebaseapp.com",
//   databaseURL: "https://riderclubadmin-22474.firebaseio.com",
//   projectId: "riderclubadmin-22474",
//   storageBucket: "riderclubadmin-22474.appspot.com",
//   messagingSenderId: "1095919617452",
//   appId: "1:1095919617452:web:e8774457c1621cf8afe609",
//   measurementId: "G-XS2D5S7DD0"
// };



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SideMenuComponent,
    DashboardComponent,
    RegisterComponent,
    LoginComponent,
    VerifyEmailComponent,
    ForgotPasswordComponent,
    
   ],
  imports: [
   
    BrowserModule, 
    AppRoutingModule,
    AngularFontAwesomeModule,
    NgxPaginationModule,
    UiSwitchModule ,
    // AngularFireModule.initializeApp(config),
    FormsModule ,
    ReactiveFormsModule ,
    HttpClientModule,
    CKEditorModule,
    BrowserAnimationsModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireDatabaseModule,
      AngularFireMessagingModule,
      AngularFireModule.initializeApp(environment.firebase),
    ToastrModule.forRoot({
      // timeOut: 800
    })
    ],
  providers: [{provide : LocationStrategy , useClass: HashLocationStrategy},{ provide: ToastrService, useClass: ToastrService},AuthService, AuthenticationGuard, DatePipe, AsyncPipe,AngularFireAuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
