import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PushNotificationMangementRoutingModule } from './pushNotificationManagement-routing.module';
import { PushNotificationComponent } from './push-notification/push-notification.component';
import { AddPushNotificationComponent } from './add-push-notification/add-push-notification.component';
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { environment } from 'src/environments/environment';
// import { AsyncPipe } from '../../node_modules/@angular/common';
import { AsyncPipe} from '../../../node_modules/@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  declarations: [PushNotificationComponent, AddPushNotificationComponent],
  imports: [
    CommonModule,
    FormsModule ,
    ReactiveFormsModule,
    PushNotificationMangementRoutingModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  
})
export class PushNotificationManagementModule { }
