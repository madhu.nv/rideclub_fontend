import { Component, OnInit } from '@angular/core';
import { ExcelServicesService } from 'src/app/Services/excel/excel-services.service';
import { PushNotificationManagementService } from 'src/app/Services/pushNotificationManagement/push-notification-management.service';


@Component({
  selector: 'app-push-notification',
  templateUrl: './push-notification.component.html',
  styleUrls: ['./push-notification.component.css']
})
export class PushNotificationComponent implements OnInit {
notifications = [];
p=1;
searchText;
  constructor( private pservice: PushNotificationManagementService,  private excelService:ExcelServicesService) { }
  excel = [];
obj = {}
  ngOnInit() {
   this.getNotifData()
  }
    getNotifData(){
        this.pservice.fetchNotif().subscribe((data: any)=>{
          this.notifications = data.response
        },(err=>{
          console.log(err);
        }))
    }

    exportAsXLSX():void {  
      this.notifications.forEach(element => {
        this.obj = {
          Title : element.title,
          Message: element.body,
          Send_To: element.status,
        }
        this.excel.push(this.obj)
      });
        this.excelService.exportAsExcelFile(this.excel, 'Push Notification');  
     }
}
