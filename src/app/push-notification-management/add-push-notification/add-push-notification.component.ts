import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PushNotificationManagementService } from 'src/app/Services/pushNotificationManagement/push-notification-management.service';

@Component({
  selector: 'app-add-push-notification',
  templateUrl: './add-push-notification.component.html',
  styleUrls: ['./add-push-notification.component.css']
})
export class AddPushNotificationComponent implements OnInit {
  message;
  submitted = false;
  pNotifForm: FormGroup = new FormGroup({})
  constructor(
    private fb: FormBuilder,
    private pNotifService: PushNotificationManagementService,
    private toastr: ToastrService,
    private actRoute: ActivatedRoute,
    private router: Router
  ) {
    this.pNotifForm = this.fb.group({
      title: new FormControl('', Validators.required),
      body: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),

    })
  }

  ngOnInit() {
    // this.messagingService.requestPermission()
    // this.messagingService.receiveMessage()
    // this.message = this.messagingService.currentMessage
  }

  onSubmit() {
    this.submitted = true;
    this.pNotifService.createNotif(this.pNotifForm.value).subscribe((data) => {
      console.log(data);
      this.toastr.success("Submitted Successfully")
      this.pNotifForm.reset()
      this.router.navigate(['/side-menu', { outlets: { sidebar: 'pushNotification' } }], { relativeTo: this.actRoute })
    }, (err => {
      console.log(err)
    }))
  }
  get f() { return this.pNotifForm.controls; }


}
