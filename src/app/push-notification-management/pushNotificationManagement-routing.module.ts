import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPushNotificationComponent } from './add-push-notification/add-push-notification.component';
import { PushNotificationComponent } from './push-notification/push-notification.component';


const routes: Routes = [
    { path: '', component: PushNotificationComponent},
    { path: 'add-push-notification', component: AddPushNotificationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PushNotificationMangementRoutingModule {}
