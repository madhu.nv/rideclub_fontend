import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RewardsmanagementService } from 'src/app/Services/rewardsManagement/rewardsmanagement.service';

@Component({
  selector: 'app-edit-bike-reward',
  templateUrl: './edit-bike-reward.component.html',
  styleUrls: ['./edit-bike-reward.component.css']
})
export class EditBikeRewardComponent implements OnInit {
  updateBikeRewardForm: FormGroup;
  id : String;
  bikeRewardsArray = [];
  constructor(private actRoute: ActivatedRoute,
    private toastr: ToastrService,
     private fb:FormBuilder, 
    private rewardService: RewardsmanagementService,
     private router: Router) { 
    this.updateBikeRewardForm = this.fb.group({
      id: [''],
      rewards: this.fb.array([]) ,
    });
  }

  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "id");
    this.fetchBikeData()
  }

  rewards() : FormArray {
    return this.updateBikeRewardForm.get("rewards") as FormArray
  }
   
  newRewards(): FormGroup {
    return this.fb.group({
      reward_details: '',
      annual_claim: '',
      points_required: ''
    })
  }
   
  addRewards() {
    this.rewards().push(this.newRewards());
  }
   
  removeReward(i:number) {
    this.rewards().removeAt(i);
  }
  fetchBikeData(){
    this.rewardService.fetchSingleBikeRewards(this.id).subscribe((data:any)=>{
      this.bikeRewardsArray = data.response[0].rewards
      console.log(this.bikeRewardsArray)
      this.updateBikeRewardForm.setControl("rewards", this.setExistingBikeRewards(this.bikeRewardsArray))
      this.updateBikeRewardForm.patchValue({
        id: this.id
      })
    })
  }
  setExistingBikeRewards(bikerewards): FormArray {
    const formArray = new FormArray([])
    bikerewards.forEach(v => {
      console.log(v, "vvvvvv")
      formArray.push(this.fb.group({
        reward_details: v. reward_details,
        annual_claim: v.annual_claim,
        points_required: v.points_required,
       
      }))

    });

    return formArray;
  }
  onSubmit(){
    this.rewardService.updateBikeRewards(this.updateBikeRewardForm.value).subscribe((data)=>{
      console.log(data);
      this.toastr.success("updated Successfully")
      this.router.navigate(['/side-menu',{outlets:{sidebar:'rewards'}}], {relativeTo: this.actRoute})
    })
  }
}
