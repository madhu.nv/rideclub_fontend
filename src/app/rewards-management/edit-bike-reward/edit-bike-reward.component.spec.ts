import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBikeRewardComponent } from './edit-bike-reward.component';

describe('EditBikeRewardComponent', () => {
  let component: EditBikeRewardComponent;
  let fixture: ComponentFixture<EditBikeRewardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBikeRewardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBikeRewardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
