import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTravellerRewardComponent } from './add-traveller-reward.component';

describe('AddTravellerRewardComponent', () => {
  let component: AddTravellerRewardComponent;
  let fixture: ComponentFixture<AddTravellerRewardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTravellerRewardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTravellerRewardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
