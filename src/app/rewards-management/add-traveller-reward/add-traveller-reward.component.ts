import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LocationManagementService } from 'src/app/Services/location-management/location-management.service';
import { RewardsmanagementService } from 'src/app/Services/rewardsManagement/rewardsmanagement.service';

@Component({
  selector: 'app-add-traveller-reward',
  templateUrl: './add-traveller-reward.component.html',
  styleUrls: ['./add-traveller-reward.component.css']
})
export class AddTravellerRewardComponent implements OnInit {
  travllerrewardForm: FormGroup;
  countries: any;
  states: any;
  selectedLevel;
  stateid
  cities: any;
  submitted = false;
  constructor(
    private toastr: ToastrService, 
    private fb:FormBuilder,
     private locationService: LocationManagementService,
      private rewardService: RewardsmanagementService, 
      private router: Router,
      private actRoute: ActivatedRoute
  ) {
    this.travllerrewardForm = this.fb.group({
      country: ['', Validators.required],
      state:['', Validators.required],
      city:['', Validators.required],
      rewards: this.fb.array([]) ,
    });
   }

  ngOnInit() {
    this.getCountry()
  }
  rewards() : FormArray {
    return this.travllerrewardForm.get("rewards") as FormArray
  }
   
  newRewards(): FormGroup {
    return this.fb.group({
      reward_details: '',
      annual_claim: '',
      points_required: ''
    })
  }
   
  addRewards() {
    this.rewards().push(this.newRewards());
  }
   
  removeReward(i:number) {
    this.rewards().removeAt(i);
  }
     
  getCountry(){
    this.locationService.fetchCountry().subscribe((data: any)=>{
      console.log(data);
      this.countries = data.data
    })
  }
  getState(){
    this.locationService.fetchSingleState(this.selectedLevel).subscribe((data:any)=>{
      console.log(data)
      this.states = data.data
    })
  }
  
  selected(){
    console.log(this.selectedLevel);
    this.getState()
  }
  getStateCity(){
    this.locationService.fetchCityById(this.stateid).subscribe((data:any)=>{
      console.log(data, "stateCoity");
      this.cities = data.data
    })
  }
  stateSelected(){
    console.log(this.stateid)
    this.getStateCity()
  }
  onSubmit(){
    this.submitted = true;
    console.log(this.travllerrewardForm.value)
    this.rewardService.createTravellerRewards({country: this.travllerrewardForm.value.country, state: this.travllerrewardForm.value.state,city: this.travllerrewardForm.value.city,rewards: this.travllerrewardForm.value.rewards}).subscribe((data)=>{
      console.log(data);
      this.toastr.success("Submitted Successfully")
      this.travllerrewardForm.reset()
      this.router.navigate(['/side-menu',{outlets:{sidebar:'rewards'}}], {relativeTo: this.actRoute})
    },(err=>{
      console.log(err);
      this.toastr.error(`${err.error.message}`)
    }))
  }
  get f() { return this.travllerrewardForm.controls; }
}
