import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RewardsmanagementService } from 'src/app/Services/rewardsManagement/rewardsmanagement.service';

@Component({
  selector: 'app-edit-reward',
  templateUrl: './edit-reward.component.html',
  styleUrls: ['./edit-reward.component.css']
})
export class EditRewardComponent implements OnInit {
  id : String;
  updateRewardForm: FormGroup;
  rewardsArray = [];
  constructor(private actRoute: ActivatedRoute,
    private toastr: ToastrService, private fb:FormBuilder, 
    private rewardService: RewardsmanagementService,
     private router: Router) { 
    this.updateRewardForm = this.fb.group({
      id: [''],
      rewards: this.fb.array([]) ,
    });
  }

  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "id");
    this.fetchData()
  }

  rewards() : FormArray {
    return this.updateRewardForm.get("rewards") as FormArray
  }
   
  newRewards(): FormGroup {
    return this.fb.group({
      reward_details: '',
      annual_claim: '',
      points_required: ''
    })
  }
   
  addRewards() {
    this.rewards().push(this.newRewards());
  }
   
  removeReward(i:number) {
    this.rewards().removeAt(i);
  }
  fetchData(){
    this.rewardService.fetchSingleRewards(this.id).subscribe((data:any)=>{
      this.rewardsArray = data.response[0].rewards
      console.log(this.rewardsArray)
      this.updateRewardForm.setControl("rewards", this.setExistingRewards(this.rewardsArray))
      this.updateRewardForm.patchValue({
        id: this.id
      })
    })
  }
  setExistingRewards(rewards): FormArray {
    const formArray = new FormArray([])
    rewards.forEach(v => {
      console.log(v, "vvvvvv")
      formArray.push(this.fb.group({
        reward_details: v. reward_details,
        annual_claim: v.annual_claim,
        points_required: v.points_required,
       
      }))

    });

    return formArray;
  }
  onSubmit(){
    this.rewardService.updateRewards(this.updateRewardForm.value).subscribe((data)=>{
      console.log(data);
      this.toastr.success("updated Successfully")
      this.router.navigate(['/side-menu',{outlets:{sidebar:'rewards'}}], {relativeTo: this.actRoute})
    })
  }
}
