import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBikeRewardComponent } from './add-bike-reward.component';

describe('AddBikeRewardComponent', () => {
  let component: AddBikeRewardComponent;
  let fixture: ComponentFixture<AddBikeRewardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBikeRewardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBikeRewardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
