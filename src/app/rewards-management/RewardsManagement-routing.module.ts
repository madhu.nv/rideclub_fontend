import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddBikeRewardComponent } from './add-bike-reward/add-bike-reward.component';
import { AddRewardComponent } from './add-reward/add-reward.component';
import { AddTravellerRewardComponent } from './add-traveller-reward/add-traveller-reward.component';
import { EditBikeRewardComponent } from './edit-bike-reward/edit-bike-reward.component';
import { EditRewardComponent } from './edit-reward/edit-reward.component';
import { EditTravellerRewardComponent } from './edit-traveller-reward/edit-traveller-reward.component';
import { RewardComponent } from './reward/reward.component';
import { ViewBikeRewardComponent } from './view-bike-reward/view-bike-reward.component';
import { ViewRewardComponent } from './view-reward/view-reward.component';
import { ViewTravellerRewardComponent } from './view-traveller-reward/view-traveller-reward.component';

const routes: Routes = [
      { path: '', component: RewardComponent},
      { path: 'add_reward', component: AddRewardComponent},
      { path: 'view_reward/:id', component: ViewRewardComponent}, 
      { path: 'edit_reward/:id', component: EditRewardComponent},
      { path: 'add_bike_reward', component: AddBikeRewardComponent},
      { path: 'view_bike_reward/:id', component: ViewBikeRewardComponent},
      {  path: 'edit_bike_reward/:id', component: EditBikeRewardComponent},
      { path: 'add_traveller_reward', component: AddTravellerRewardComponent},
      { path: 'view_traveller_reward/:id', component: ViewTravellerRewardComponent},
      {  path: 'edit_traveller_reward/:id', component: EditTravellerRewardComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RewardsMangementRoutingModule {}
