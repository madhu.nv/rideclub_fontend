import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RewardsmanagementService } from 'src/app/Services/rewardsManagement/rewardsmanagement.service';

@Component({
  selector: 'app-view-traveller-reward',
  templateUrl: './view-traveller-reward.component.html',
  styleUrls: ['./view-traveller-reward.component.css']
})
export class ViewTravellerRewardComponent implements OnInit {
  id : String;
  travellerRewardsArray;
  p=1
  constructor(private actRoute: ActivatedRoute, 
    private rewardService: RewardsmanagementService) { }

  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "id");
    this.fetchTravellerData();
  }
  fetchTravellerData(){
    this.rewardService.fetchSingleTravellerRewards(this.id).subscribe((data:any)=>{
      this.travellerRewardsArray = data.response[0].rewards
      console.log(this.travellerRewardsArray)
    })
  }
}
