import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTravellerRewardComponent } from './view-traveller-reward.component';

describe('ViewTravellerRewardComponent', () => {
  let component: ViewTravellerRewardComponent;
  let fixture: ComponentFixture<ViewTravellerRewardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTravellerRewardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTravellerRewardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
