import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RewardsmanagementService } from 'src/app/Services/rewardsManagement/rewardsmanagement.service';

@Component({
  selector: 'app-view-reward',
  templateUrl: './view-reward.component.html',
  styleUrls: ['./view-reward.component.css']
})
export class ViewRewardComponent implements OnInit {
  id : String;
  rewardsArray = [];
  p=1;
  constructor(private actRoute: ActivatedRoute, private rewardService: RewardsmanagementService) { }

  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "id");
    this.fetchData()
  }
    fetchData(){
      this.rewardService.fetchSingleRewards(this.id).subscribe((data:any)=>{
        this.rewardsArray = data.response[0].rewards
        console.log(this.rewardsArray)
      })
    }
}
