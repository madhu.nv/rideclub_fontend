import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTravellerRewardComponent } from './edit-traveller-reward.component';

describe('EditTravellerRewardComponent', () => {
  let component: EditTravellerRewardComponent;
  let fixture: ComponentFixture<EditTravellerRewardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTravellerRewardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTravellerRewardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
