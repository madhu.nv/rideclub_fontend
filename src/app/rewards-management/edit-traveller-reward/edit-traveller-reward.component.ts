import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RewardsmanagementService } from 'src/app/Services/rewardsManagement/rewardsmanagement.service';

@Component({
  selector: 'app-edit-traveller-reward',
  templateUrl: './edit-traveller-reward.component.html',
  styleUrls: ['./edit-traveller-reward.component.css']
})
export class EditTravellerRewardComponent implements OnInit {
  id : String;
  updateTravellerRewardForm: FormGroup;
  travellerrewardsArray = [];
  constructor(private actRoute: ActivatedRoute,
    private toastr: ToastrService, private fb:FormBuilder, 
    private rewardService: RewardsmanagementService,
     private router: Router) { 
      this.updateTravellerRewardForm = this.fb.group({
        id: [''],
        rewards: this.fb.array([]) ,
      });
     }

  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "id");
    this.fetchTravellerData();
  }
    
  rewards() : FormArray {
    return this.updateTravellerRewardForm.get("rewards") as FormArray
  }
   
  newRewards(): FormGroup {
    return this.fb.group({
      reward_details: '',
      annual_claim: '',
      points_required: ''
    })
  }
   
  addRewards() {
    this.rewards().push(this.newRewards());
  }
   
  removeReward(i:number) {
    this.rewards().removeAt(i);
  }
  fetchTravellerData(){
    this.rewardService.fetchSingleTravellerRewards(this.id).subscribe((data:any)=>{
      this.travellerrewardsArray = data.response[0].rewards
      console.log(this.travellerrewardsArray)
      this.updateTravellerRewardForm.setControl("rewards", this.setExistingRewards(this.travellerrewardsArray))
      this.updateTravellerRewardForm.patchValue({
        id: this.id
      })
    })
  }
  setExistingRewards(trewards): FormArray {
    const formArray = new FormArray([])
    trewards.forEach(v => {
      console.log(v, "vvvvvv")
      formArray.push(this.fb.group({
        reward_details: v. reward_details,
        annual_claim: v.annual_claim,
        points_required: v.points_required,
       
      }))

    });

    return formArray;
  }
  onSubmit(){
    this.rewardService.updateTravellerRewards(this.updateTravellerRewardForm.value).subscribe((data)=>{
      console.log(data);
      this.toastr.success("updated Successfully")
      this.router.navigate(['/side-menu',{outlets:{sidebar:'rewards'}}], {relativeTo: this.actRoute})
    })
  }
}
