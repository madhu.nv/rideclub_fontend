import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RewardComponent } from './reward/reward.component';
import { RewardsMangementRoutingModule } from './RewardsManagement-routing.module';
import { AddRewardComponent } from './add-reward/add-reward.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ViewRewardComponent } from './view-reward/view-reward.component';
import { EditRewardComponent } from './edit-reward/edit-reward.component';
import { AddBikeRewardComponent } from './add-bike-reward/add-bike-reward.component';
import { ViewBikeRewardComponent } from './view-bike-reward/view-bike-reward.component';
import { EditBikeRewardComponent } from './edit-bike-reward/edit-bike-reward.component';
import { AddTravellerRewardComponent } from './add-traveller-reward/add-traveller-reward.component';
import { ViewTravellerRewardComponent } from './view-traveller-reward/view-traveller-reward.component';
import { EditTravellerRewardComponent } from './edit-traveller-reward/edit-traveller-reward.component';



@NgModule({
  declarations: [RewardComponent, AddRewardComponent, ViewRewardComponent, EditRewardComponent, AddBikeRewardComponent, ViewBikeRewardComponent, EditBikeRewardComponent, AddTravellerRewardComponent, ViewTravellerRewardComponent, EditTravellerRewardComponent],
  imports: [
    CommonModule,
    RewardsMangementRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    NgxPaginationModule,
    Ng2SearchPipeModule, 
  ]
})
export class RewardsManagementModule { }
