import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LocationManagementService } from 'src/app/Services/location-management/location-management.service';
import { RewardsmanagementService } from 'src/app/Services/rewardsManagement/rewardsmanagement.service';

@Component({
  selector: 'app-add-reward',
  templateUrl: './add-reward.component.html',
  styleUrls: ['./add-reward.component.css']
})
export class AddRewardComponent implements OnInit {
  rewardForm: FormGroup;
  countries: any;
  states: any;
  selectedLevel;
  stateid
  cities: any;
  submitted = false;
  constructor( private toastr: ToastrService, 
    private fb:FormBuilder,
     private locationService: LocationManagementService,
      private rewardService: RewardsmanagementService, 
      private router: Router,
      private actRoute: ActivatedRoute) { 
    this.rewardForm = this.fb.group({
      country: ['', Validators.required],
      state:['',  Validators.required],
      city:['',  Validators.required],
      rewards: this.fb.array([]) ,
    });
  }

  ngOnInit() {
    this.getCountry()
  }
  rewards() : FormArray {
    return this.rewardForm.get("rewards") as FormArray
  }
   
  newRewards(): FormGroup {
    return this.fb.group({
      reward_details: '',
      annual_claim: '',
      points_required: ''
    })
  }
   
  addRewards() {
    this.rewards().push(this.newRewards());
  }
   
  removeReward(i:number) {
    this.rewards().removeAt(i);
  }
   
  getCountry(){
    this.locationService.fetchCountry().subscribe((data: any)=>{
      console.log(data);
      this.countries = data.data
    })
  }
  getState(){
    this.locationService.fetchSingleState(this.selectedLevel).subscribe((data:any)=>{
      console.log(data)
      this.states = data.data
    })
  }
  
  selected(){
    console.log(this.selectedLevel);
    this.getState()
  }
  getStateCity(){
    this.locationService.fetchCityById(this.stateid).subscribe((data:any)=>{
      console.log(data, "stateCoity");
      this.cities = data.data
    })
  }
  stateSelected(){
    console.log(this.stateid)
    this.getStateCity()
  }
  onSubmit() {
    this.submitted = true;
    console.log(this.rewardForm.value)
   this.rewardService.createRewards({country: this.rewardForm.value.country, state: this.rewardForm.value.state,city: this.rewardForm.value.city,rewards: this.rewardForm.value.rewards}).subscribe((data)=>{
     console.log(data);
     this.toastr.success("Submitted Successfully")
     this.rewardForm.reset()
     this.router.navigate(['/side-menu',{outlets:{sidebar:'rewards'}}], {relativeTo: this.actRoute})
   },(err=>{
     console.log(err);
     this.toastr.error(`${err.error.message}`)
   }))
  }
  get f() { return this.rewardForm.controls; }
}
