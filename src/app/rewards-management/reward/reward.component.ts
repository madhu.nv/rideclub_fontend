import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ExcelServicesService } from 'src/app/Services/excel/excel-services.service';
import { RewardsmanagementService } from 'src/app/Services/rewardsManagement/rewardsmanagement.service';


@Component({
  selector: 'app-reward',
  templateUrl: './reward.component.html',
  styleUrls: ['./reward.component.css']
})
export class RewardComponent implements OnInit {
  p=1;
rewards = [];
searchText;
bikeRewards = [];
travellerRewards = [];
carExcel = []
excel = [];
bikeExcel = []
obj = {}
bikeobj = {}
carobj = {}
  constructor(private rewardService: RewardsmanagementService, private excelService:ExcelServicesService) { }

  ngOnInit() {
    
    this.fetchRewardsData();
    this.fetchBikeRewardsData();
    this.fetchTravellerRewardsData();
  }
  fetchRewardsData(){
    this.rewardService.fetchRewards().subscribe((data:any)=>{
      console.log(data);
      this.rewards = data.response
    }, (err=>{
      console.log(err);
    }))
  }
  deleteReward(id){
    console.log(id, "ffff")
   
      var r = confirm("Do you really want to delete this record?");
      if(r==true){
        this.rewardService.deleteRewards(id).subscribe((data)=>{
          console.log(data);
          this.fetchRewardsData()
        },(err=>{
          console.log(err)
        }))
      }else{
        console.log("error")
      }
   }


   //Bike Rewards
   fetchBikeRewardsData(){
    this.rewardService.fetchBikeRewards().subscribe((data:any)=>{
      this.bikeRewards = data.response
      console.log(data);
    }, (err=>{
      console.log(err);
    }))
  }
  deleteBikereward(id){
    var r = confirm("Do you really want to delete this record?");
    if(r==true){
      this.rewardService.deleteBikeRewards(id).subscribe((data)=>{
        console.log(data);
        this.fetchBikeRewardsData()
      },(err=>{
        console.log(err)
      }))
    }else{
      console.log("error")
    }
  }

  //Traveller Rewards
  fetchTravellerRewardsData(){
    this.rewardService.fetchTravellerRewards().subscribe((data:any)=>{
      this.travellerRewards = data.response
      console.log(data);
    }, (err=>{
      console.log(err);
    }))
  }

  deleteTData(id){
    console.log(id, "deletee")
    var r = confirm("Do you really want to delete this record?");
    if(r==true){
      this.rewardService.deleteTravellerRewards(id).subscribe((data)=>{
        console.log(data);
        this.fetchTravellerRewardsData()
      },(err=>{
        console.log(err)
      }))
    }else{
      console.log("error")
    }
  }
  exportAsXLSX():void {  
    this.rewards.forEach(element => {
      element.rewards.forEach(element1 => {
        console.log(element1)
          this.obj = {
        country: element.country,
       state: element.state,
       city: element.city,
       annual_claim: element1.annual_claim,
       points_required: element1.points_required,
       reward_details: element1.reward_details
      }
        this.excel.push(this.obj)
      });
    });
      this.excelService.exportAsExcelFile(this.excel, 'Car Rewards');  
   }

   exportBikeAsXLSX():void{
    this.bikeRewards.forEach(element => {
      element.rewards.forEach(element1 => {
        console.log(element1)
          this.bikeobj = {
        Country: element.country,
       State: element.state,
       City: element.city,
       Annualclaim: element1.annual_claim,
       PointsRequired: element1.points_required,
       RewardDetails: element1.reward_details
      }
        this.bikeExcel.push(this.bikeobj)
      });
    });
      this.excelService.exportAsExcelFile(this.bikeExcel, 'Bike Rewards');  
   }
   exportCarAsXLSX(){
    this.travellerRewards.forEach(element => {
      element.rewards.forEach(element1 => {
        console.log(element1)
          this.carobj = {
        Country: element.country,
       State: element.state,
       City: element.city,
       Annualclaim: element1.annual_claim,
       PointsRequired: element1.points_required,
       RewardDetails: element1.reward_details
      }
        this.carExcel.push(this.carobj)
      });
    });
      this.excelService.exportAsExcelFile(this.carExcel, 'Traveller Rewards'); 
   }
}

