import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RewardsmanagementService } from 'src/app/Services/rewardsManagement/rewardsmanagement.service';

@Component({
  selector: 'app-view-bike-reward',
  templateUrl: './view-bike-reward.component.html',
  styleUrls: ['./view-bike-reward.component.css']
})
export class ViewBikeRewardComponent implements OnInit {
  p=1;
  id : String;
  bikeRewardsArray = [];
  constructor(private actRoute: ActivatedRoute, private rewardService: RewardsmanagementService) { }

  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "id");
    this.fetchBikeData();
  }
  fetchBikeData(){
    this.rewardService.fetchSingleBikeRewards(this.id).subscribe((data:any)=>{
      this.bikeRewardsArray = data.response[0].rewards
      console.log(this.bikeRewardsArray, "bikes Array")
    })
  }
}
