import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBikeRewardComponent } from './view-bike-reward.component';

describe('ViewBikeRewardComponent', () => {
  let component: ViewBikeRewardComponent;
  let fixture: ComponentFixture<ViewBikeRewardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBikeRewardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBikeRewardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
