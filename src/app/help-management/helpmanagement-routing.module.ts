import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HelpComponent } from './help/help.component';
import { UpdateHelpDataComponent } from './update-help-data/update-help-data.component';
import { ViewHelpManagementComponent } from './view-help-management/view-help-management.component';
import { AddHelpComponent } from './add-help/add-help.component';




const routes: Routes = [
    { path: "", component:HelpComponent},
    { path: "update_help/:id", component: UpdateHelpDataComponent},
    { path: "view_help/:id", component:ViewHelpManagementComponent},
    { path: "add_help", component:AddHelpComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HelpMangementRoutingModule { }