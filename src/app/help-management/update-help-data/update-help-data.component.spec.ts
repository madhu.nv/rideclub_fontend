import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateHelpDataComponent } from './update-help-data.component';

describe('UpdateHelpDataComponent', () => {
  let component: UpdateHelpDataComponent;
  let fixture: ComponentFixture<UpdateHelpDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateHelpDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateHelpDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
