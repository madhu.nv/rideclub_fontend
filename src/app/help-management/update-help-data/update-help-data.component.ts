import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { HelpManagementService } from 'src/app/Services/helpManagement/help-management.service';

@Component({
  selector: 'app-update-help-data',
  templateUrl: './update-help-data.component.html',
  styleUrls: ['./update-help-data.component.css']
})
export class UpdateHelpDataComponent implements OnInit {
  help_id:any;
  editHelpDataForm: FormGroup = new FormGroup({})
  public Editor = ClassicEditor;
  ckeConfig: any;  
  fetchDataArray: any;
  submitted = false;
  constructor(private actRoute: ActivatedRoute, private fb: FormBuilder, private helpDataService: HelpManagementService, private router: Router) { 
    this.editHelpDataForm = this.fb.group({
      help_data: new FormControl('', Validators.required),
     
    })
  }

  ngOnInit() {
    this.help_id = this.actRoute.snapshot.params.id
    console.log(this.help_id, "helpId")
    this.fetchHelpDataById()
  
  }

  fetchHelpDataById(){
    this.helpDataService.fetchById(this.help_id).subscribe((data:any)=>{
      this.fetchDataArray = data.response.help_data
      console.log(this.fetchDataArray)
      this.editHelpDataForm.patchValue({
        help_data : this.fetchDataArray
      })
    },(err=>{
      console.log(err)
    }))
  }

  get f() { return this.editHelpDataForm.controls}

  onSubmit(){
    this.submitted = true;
    this.helpDataService.updateHelp(this.editHelpDataForm.value).subscribe((data)=>{
      console.log(data, "update help data")
      this.editHelpDataForm.reset()
      this.router.navigate(['/side-menu',{outlets:{sidebar:'help'}}], {relativeTo: this.actRoute})
    }, (err=>{
      console.log(err)
    }))
  }

}
