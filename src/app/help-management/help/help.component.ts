import { Component, OnInit } from '@angular/core';
import { HelpManagementService } from 'src/app/Services/helpManagement/help-management.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {
fetchHelpArray: any;
id:any;
  constructor( private helpDataService: HelpManagementService) { }

  ngOnInit() {
    this.fetchHelpData()
  }

fetchHelpData(){
  this.helpDataService.fetchHelp().subscribe((data: any)=>{
    this.fetchHelpArray = data.response[0].help_data
    this.id = data.response[0]._id
    console.log(this.id)
    console.log(this.fetchHelpArray, "help data")
  }, (err)=>{
    console.log(err)
  })
}

}
