import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HelpManagementService } from 'src/app/Services/helpManagement/help-management.service';

@Component({
  selector: 'app-add-help',
  templateUrl: './add-help.component.html',
  styleUrls: ['./add-help.component.css']
})
export class AddHelpComponent implements OnInit {
  ckeConfig: any;  
  public Editor = ClassicEditor;
  addHelpForm: FormGroup = new FormGroup({})
  constructor(private fb: FormBuilder, private helpDataService: HelpManagementService) { 
    this.addHelpForm = this.fb.group({
      help_data: new FormControl('', [Validators.required])

    })
  }

  ngOnInit() {
    this.ckeConfig = {    
      allowedContent: false,    
      // extraPlugins: 'divarea',    
      forcePasteAsPlainText: true    
    };    
  }
  submitAddHelp(){
    
    this.helpDataService.createHelp(this.addHelpForm.value).subscribe((data)=>{
      console.log(data, "posted data successfully")
      this.addHelpForm.reset()
    })
  }
}
