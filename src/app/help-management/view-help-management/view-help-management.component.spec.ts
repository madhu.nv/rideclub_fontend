import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewHelpManagementComponent } from './view-help-management.component';

describe('ViewHelpManagementComponent', () => {
  let component: ViewHelpManagementComponent;
  let fixture: ComponentFixture<ViewHelpManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewHelpManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewHelpManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
