import { Component, OnInit } from '@angular/core';
import { HelpManagementService } from 'src/app/Services/helpManagement/help-management.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-help-management',
  templateUrl: './view-help-management.component.html',
  styleUrls: ['./view-help-management.component.css']
})
export class ViewHelpManagementComponent implements OnInit {
  help_id:any;
  fetchArray: any;
  constructor(private helpDataService: HelpManagementService, private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.help_id = this.actRoute.snapshot.params.id
    console.log(this.help_id, "helpId")
    this.fetchData()
  }

fetchData(){
  this.helpDataService.fetchById(this.help_id).subscribe((data:any)=>{
    console.log(data)
    this.fetchArray = data.response.help_data
  })
}

}
