import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelpMangementRoutingModule } from './helpmanagement-routing.module';
import { HelpComponent } from './help/help.component';
import { UpdateHelpDataComponent } from './update-help-data/update-help-data.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ViewHelpManagementComponent } from './view-help-management/view-help-management.component';
import { AddHelpComponent } from './add-help/add-help.component';



@NgModule({
  declarations: [HelpComponent, UpdateHelpDataComponent, ViewHelpManagementComponent, AddHelpComponent],
  imports: [
    CommonModule,
    HelpMangementRoutingModule,
    FormsModule ,
    ReactiveFormsModule,
    CKEditorModule
  ]
})
export class HelpManagementModule { }
