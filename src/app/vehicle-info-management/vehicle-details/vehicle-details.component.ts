import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { VehicleManagementService } from 'src/app/Services/vehicleManagement/vehicle-management.service';

@Component({
  selector: 'app-vehicle-details',
  templateUrl: './vehicle-details.component.html',
  styleUrls: ['./vehicle-details.component.css']
})
export class VehicleDetailsComponent implements OnInit {
  productForm: FormGroup;
  coloursForm: FormGroup;
  fuelTypeForm: FormGroup;
  images;
  imagesbrand = [];
  someData: any;
  ggg = []
array:any
submitted = false;
someArray = [];
filesToUpload : any;
filesArray= [];
numberPattern = "^((\\+91-?)|0)?[0-9]$"; 
url;
brandUrl = [];
  constructor(private fb: FormBuilder, private vehicleService: VehicleManagementService,  private actRoute: ActivatedRoute, 
    private router: Router, private toastr: ToastrService) {
    this.productForm = this.fb.group({
      vehicleType:[ '', Validators.required],
      noOfSeats: [''],
      vehicledetails: this.fb.array([this.newVehicle()])
    });
  }

  vehicledetails(): FormArray {
    return this.productForm.get("vehicledetails") as FormArray
  }

  newVehicle(): FormGroup {
    return this.fb.group({
      vehicleBrand: ['', Validators.required],
      seats: ['', [Validators.required,Validators.pattern(this.numberPattern),
      Validators.minLength(1),]],
      kmperLtr: ['', Validators.required],
      uploads: [''],
      colours: this.fb.array([ new FormControl]),
      fuelType: this.fb.array([new FormControl])
    })
}



// initImagesData() {
//   return this.fb.group({
//     images: new FormControl('', [
//       Validators.required,
//     ]),
//   });
// }

uploadsDetails(empIndex:number): FormArray{
  return this.vehicledetails().at(empIndex).get('uploads') as FormArray
}
newData(){
  return this.fb.group({
    images_data: ''
  })
}

coloursDetails(empIndex:number): FormArray {
  return this.vehicledetails().at(empIndex).get('colours') as FormArray
}
  newcolours(){
  return this.fb.group([
    new FormControl()
   ])
}
addColours(empIndex:number){
  this.coloursDetails(empIndex).push(new FormControl())
  
}
removeColour(i,j){
  this.coloursDetails(i).removeAt(j);
}

fuelDetails(empIndex:number): FormArray{
  return this.vehicledetails().at(empIndex).get('fuelType') as FormArray
}
addFuel(empIndex:number){
  this.fuelDetails(empIndex).push(new FormControl())
  
}
removeFuel(i,k){
  this.fuelDetails(i).removeAt(k);
}
  addVehicle() {
   
    this.vehicledetails().push(this.newVehicle());

  }

  removeVehicle(i: number) {
    this.vehicledetails().removeAt(i);
  }

  get f() {
    return this.productForm.controls;
    }


  ngOnInit() {}

  handleEvent(event){
    if(event.target.files.length > 0){
      const file = event.target.files[0];
      this.images = file
      
    }
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
			this.url = reader.result; 
		}
  }

  selectEvent(event,i){
   
    // if(event.target.files.length > 0){
      this.filesToUpload = event.target.files as Array<File>;
      console.log(this.filesToUpload, "upload")
      this.filesArray[i] = this.filesToUpload[0]
      let files = event.target.files;
      let filename1 = files[0].name;
      if(files.length === 0){
        return
      }
      console.log(files)
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (_event) => {
        this.brandUrl[i] = reader.result; 
      }
  }

  onSubmit() { 
    this.submitted = true;
    console.log(this.productForm.value, "nnnnnnn")
  this.array = this.productForm.value.vehicledetails
    const formData:any = new FormData();
    const files = this.filesArray;
    let keys = Object.keys(files);

    let lithLen = this.productForm.value.vehicledetails.length;
    if (lithLen > 0 && files) {
      for (let i = 0; i < lithLen; i++) {
        this.productForm.value.vehicledetails[i].uploads = files[i].name;
        formData.append("uploads[]", files[i], files[i].name);
      }
    }
    formData.append('vehicleimageData',this.images);
    formData.append('noOfSeats',this.productForm.value.noOfSeats);
    formData.append('vehicleType',this.productForm.value.vehicleType);
    formData.append('vehicledetails',JSON.stringify(this.array));
    
    // formData.append('brandLogo[]', this.imagesbrand)
    // formData.append('colours',JSON.stringify(this.productForm.value.colours));
    // formData.append('fuelType',JSON.stringify(this.productForm.value.fuelType));
    this.vehicleService.createVehicleType(formData).subscribe((data) => {
      console.log(data, "data")
      this.productForm.reset();
      this.router.navigate(['/side-menu',{outlets:{sidebar:'vehicleInfo'}}], {relativeTo: this.actRoute})
    }, (err => {
      console.log(err)
      this.toastr.error(`${err.error.message}`)
    }))
  }
}
