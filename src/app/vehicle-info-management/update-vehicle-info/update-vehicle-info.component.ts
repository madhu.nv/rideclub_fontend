import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { VehicleManagementService } from 'src/app/Services/vehicleManagement/vehicle-management.service';

@Component({
  selector: 'app-update-vehicle-info',
  templateUrl: './update-vehicle-info.component.html',
  styleUrls: ['./update-vehicle-info.component.css']
})
export class UpdateVehicleInfoComponent implements OnInit {
  id: any
  updateproductForm: FormGroup;
  images;
  someData: any;
  array: any
  brandlogo: any
  updateDataArray: any
  someImage: any;
  image: any;
  submitted
  noOfSeats: any
  filesToUpload: any;
  filesArray = [];
  veh: any;
  imagesArray = [];
  isDataAvailable: boolean = false;
  constructor(private fb: FormBuilder,
    private vehicleService: VehicleManagementService,
    private actRoute: ActivatedRoute,
    private router: Router) {

    this.updateproductForm = this.fb.group({
      id: [''],
      vehicleType: ['', Validators.required],
      vehicleimageData: [''],
      noOfSeats: [''],
      vehicledetails: this.fb.array([this.newVehicle()])
    });
  }

  vehicledetails(): FormArray {
    return this.updateproductForm.get("vehicledetails") as FormArray
  }

  newVehicle(): FormGroup {
    return this.fb.group({
      vehicleBrand: ['', Validators.required],
      seats: ['', [Validators.required, Validators.pattern("^[0-9]*$"),
      Validators.minLength(3),]],
      kmperLtr: ['', Validators.required],
      uploads: [''],
      colours: this.fb.array([new FormControl]),
      fuelType: this.fb.array([new FormControl])
    })
  }

  uploadsDetails(empIndex: number): FormArray {
    return this.vehicledetails().at(empIndex).get('uploads') as FormArray
  }
  newData() {
    return this.fb.group({
      images_data: ''
    })
  }

  coloursDetails(empIndex: number): FormArray {
    return this.vehicledetails().at(empIndex).get('colours') as FormArray
  }
  newcolours() {
    return this.fb.group([
      new FormControl()
    ])
  }
  addColours(empIndex: number) {
    this.coloursDetails(empIndex).push(new FormControl())

  }
  removeColour(i, j) {
    this.coloursDetails(i).removeAt(j);
  }

  fuelDetails(empIndex: number): FormArray {
    return this.vehicledetails().at(empIndex).get('fuelType') as FormArray
  }
  addFuel(empIndex: number) {
    this.fuelDetails(empIndex).push(new FormControl())

  }
  removeFuel(i, k) {
    this.fuelDetails(i).removeAt(k);
  }
  addVehicle() {
    this.vehicledetails().push(this.newVehicle());
  }

  removeVehicle(i: number) {
    this.vehicledetails().removeAt(i);
  }

  get f() {
    return this.updateproductForm.controls;
  }


  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "docID")
    this.getVehicledatabyId()
  }

  handleEvent(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.images = file
    }
  }

  getVehicledatabyId() {
    console.log(this.id)
    this.vehicleService.getVehicleData(this.id).subscribe((updateData: any) => {
      this.imagesArray = updateData.data.vehicledetails;
      this.updateDataArray = updateData.data
      this.image = updateData.data.vehicleimageData
      this.id = updateData.data._id
      this.noOfSeats = updateData.data.noOfSeats.length
      console.log(this.updateDataArray, "nnnnn")
      this.isDataAvailable = true
      this.updateproductForm.setControl("vehicledetails", this.setExistingVehicleDetails(updateData.data.vehicledetails))
      this.updateproductForm.patchValue({
        vehicleimageData: this.image,
        vehicleType: this.updateDataArray.vehicleType,
        id: this.id,
        noOfSeats: this.noOfSeats
      })
    }, (err => {
      console.log(err)
    }))
  }

  setExistingVehicleDetails(vehicledetails): FormArray {
    const formArray = new FormArray([])
    vehicledetails.forEach(v => {
      formArray.push(this.fb.group({
        vehicleBrand: v.vehicleBrand,
        seats: v.seats,
        kmperLtr: v.kmperLtr,
        uploads: v.uploads,
        colours: this.fb.array(v.colours),
        fuelType: this.fb.array(v.fuelType)
      }))
    });
    return formArray;
  }

  selectEvent(event, i) {
    // if(event.target.files.length > 0){
    this.filesToUpload = event.target.files as Array<File>;
    console.log(this.filesToUpload, "upload")
    this.filesArray[i] = this.filesToUpload[0]
    let files = event.target.files;
    let filename1 = files[0].name;
    if (files.length === 0) {
      return
    }
    console.log(files, "files")
  }

  onSubmit() {
    console.log(this.updateproductForm.value, "value")
    this.array = this.updateproductForm.value.vehicledetails
    const formData: any = new FormData();
    const files = this.filesArray;
    let keys = Object.keys(files);
    // let lithLen = this.updateproductForm.value.vehicledetails.length;
    let lithLen = this.filesArray.length
    console.log(this.filesArray, "filesArray");
    console.log(lithLen, "lllllllll")
    if (lithLen > 0 && files) {
      for (let i = 0; i < lithLen; i++) {
        if(files[i]){
           this.updateproductForm.value.vehicledetails[i].uploads = files[i].name;
        formData.append("uploads[]", files[i], files[i].name);
        }else{
          console.log("not exist");
        }

      
      }
    }
    // formData.append('imageee', this.image);
    formData.append('id', this.id);
    formData.append('vehicleimageData', this.images);
    formData.append('noOfSeats', this.updateproductForm.value.noOfSeats);
    formData.append('vehicleType', this.updateproductForm.value.vehicleType);
    formData.append('vehicledetails', JSON.stringify(this.array));
    this.vehicleService.updateData(formData).subscribe((data) => {
      console.log(data, "data")
      this.updateproductForm.reset();
      this.router.navigate(['/side-menu', { outlets: { sidebar: 'vehicleInfo' } }], { relativeTo: this.actRoute })
    }, (err => {
      console.log(err)
    }))
    // }
  }
}
