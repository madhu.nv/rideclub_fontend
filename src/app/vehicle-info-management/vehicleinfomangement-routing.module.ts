import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VehicleDetailsComponent } from './vehicle-details/vehicle-details.component';
import { ViewVehicleInfoManagementComponent } from './view-vehicle-info-management/view-vehicle-info-management.component';
import { VehicleDataComponent } from './vehicle-data/vehicle-data.component';
import { UpdateVehicleInfoComponent } from './update-vehicle-info/update-vehicle-info.component';


const routes: Routes = [
   { path: '', component: ViewVehicleInfoManagementComponent},
    { path: 'vehicle-info', component: VehicleDetailsComponent},
    { path: 'vehicle_data/:id', component: VehicleDataComponent},
    { path: 'update_vehicle_data/:id', component: UpdateVehicleInfoComponent}
    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleInfoMangementRoutingModule { }
