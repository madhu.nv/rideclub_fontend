import { Component, OnInit } from '@angular/core';
import { VehicleManagementService } from 'src/app/Services/vehicleManagement/vehicle-management.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-view-vehicle-info-management',
  templateUrl: './view-vehicle-info-management.component.html',
  styleUrls: ['./view-vehicle-info-management.component.css']
})
export class ViewVehicleInfoManagementComponent implements OnInit {
image1:any;
searchText;
p: number = 1;
collection = [];
  constructor( private vehicleViewService: VehicleManagementService, private router: Router, private route: ActivatedRoute) { 
    // for (let i = 1; i <= 100; i++) {
    //   this.collection.push(`vehicle ${i}`);
    // }
  }

  ngOnInit() {
    this.getVehicleImage()
}

getVehicleImage(){
  this.vehicleViewService.getImage().subscribe((data:any)=>{  
    console.log(data)
  this.image1 = data.data
  console.log(this.image1)
 },(err=>{
    console.log(err)
  }))
}

deleteData(id){
  console.log(id)
  var r = confirm("Do you really want to delete this record?")
  if(r == true){
    this.vehicleViewService.deleteData(id).subscribe((deletedData)=>{
      console.log(deletedData)
      this.getVehicleImage()
    }, (err=>{
      console.log(err)
    }))
  }else{
    console.log("cancelled")
  }
 
}

updateData(vehicle){
  console.log(vehicle)
  this.router.navigate(['./update_vehicle_data', vehicle._id], {relativeTo: this.route})
}

}
