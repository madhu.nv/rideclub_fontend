import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewVehicleInfoManagementComponent } from './view-vehicle-info-management.component';

describe('ViewVehicleInfoManagementComponent', () => {
  let component: ViewVehicleInfoManagementComponent;
  let fixture: ComponentFixture<ViewVehicleInfoManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewVehicleInfoManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewVehicleInfoManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
