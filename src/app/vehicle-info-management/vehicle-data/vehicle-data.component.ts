import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VehicleManagementService } from 'src/app/Services/vehicleManagement/vehicle-management.service';

@Component({
  selector: 'app-vehicle-data',
  templateUrl: './vehicle-data.component.html',
  styleUrls: ['./vehicle-data.component.css']
})
export class VehicleDataComponent implements OnInit {
  id: any
  getColoursArray: any;
  getFuelsArray: any;
  getVehicalModels: any;
  noOfSeats:any;
 imageArray = [];
  constructor(private actRoute: ActivatedRoute, private service:VehicleManagementService) { 
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "docID")
  }

  ngOnInit() {
    this.geVehicleData(this.id)
  }

  geVehicleData(id){
    this.service.getVehicleData(id).subscribe((vehicleData:any)=>{
      console.log(vehicleData.data, "vehicle Data")
      this.getColoursArray = vehicleData.data.vehicledetails
      console.log(this.getColoursArray, "colours")
      this.imageArray = vehicleData.data.vehicledetails[0].brandlogo
      console.log(this.imageArray, "image")
      this.getFuelsArray = vehicleData.data.vehicledetails
      this.getVehicalModels = vehicleData.data.vehicledetails
      this.noOfSeats = vehicleData.data.noOfSeats.length
      console.log(this.noOfSeats, "djhdjh")
    })
  }

 

}
