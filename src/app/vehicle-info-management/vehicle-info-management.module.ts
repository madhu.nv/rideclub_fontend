import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VehicleInfoMangementRoutingModule } from './vehicleinfomangement-routing.module';
import { VehicleDetailsComponent } from './vehicle-details/vehicle-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewVehicleInfoManagementComponent } from './view-vehicle-info-management/view-vehicle-info-management.component';
import { VehicleDataComponent } from './vehicle-data/vehicle-data.component';
import { UpdateVehicleInfoComponent } from './update-vehicle-info/update-vehicle-info.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';



@NgModule({
  declarations: [VehicleDetailsComponent, ViewVehicleInfoManagementComponent, VehicleDataComponent, UpdateVehicleInfoComponent],
  imports: [
    CommonModule,
    VehicleInfoMangementRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule
  ]
})
export class VehicleInfoManagementModule { }
