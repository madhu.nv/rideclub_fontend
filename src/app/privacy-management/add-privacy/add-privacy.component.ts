import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { PrivacypolicymanagementService } from 'src/app/Services/privacypolicymanagement/privacypolicymanagement.service';
@Component({
  selector: 'app-add-privacy',
  templateUrl: './add-privacy.component.html',
  styleUrls: ['./add-privacy.component.css']
})
export class AddPrivacyComponent implements OnInit {
  ckeConfig: any;  
  public Editor = ClassicEditor;
  addPrivacyForm: FormGroup = new FormGroup({})
  constructor(private fb: FormBuilder, private privacypService: PrivacypolicymanagementService) { 
    this.addPrivacyForm = this.fb.group({
      privacy_policy_data: new FormControl('', [Validators.required])

    })
  }

  ngOnInit() {
    this.ckeConfig = {    
      allowedContent: false,    
      // extraPlugins: 'divarea',    
      forcePasteAsPlainText: true    
    };   
   
  }
  submitAddPrivacy(){
    this.privacypService.craetePrivacy(this.addPrivacyForm.value).subscribe((data)=>{
      console.log(data, "posted data successfully")
      this.addPrivacyForm.reset()
    })
  }

}
