import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePrivacyPolicyDataComponent } from './update-privacy-policy-data.component';

describe('UpdatePrivacyPolicyDataComponent', () => {
  let component: UpdatePrivacyPolicyDataComponent;
  let fixture: ComponentFixture<UpdatePrivacyPolicyDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePrivacyPolicyDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePrivacyPolicyDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
