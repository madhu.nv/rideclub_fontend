import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { PrivacypolicymanagementService } from 'src/app/Services/privacypolicymanagement/privacypolicymanagement.service';

@Component({
  selector: 'app-update-privacy-policy-data',
  templateUrl: './update-privacy-policy-data.component.html',
  styleUrls: ['./update-privacy-policy-data.component.css']
})
export class UpdatePrivacyPolicyDataComponent implements OnInit {
  privacyp_id:any;
  public Editor = ClassicEditor;
  ckeConfig: any;  
  submitted = false;
  fetchprivacyDataArray: any;
  editPrivacypDataForm: FormGroup = new FormGroup({})
  constructor(private actRoute: ActivatedRoute, private privacyPService: PrivacypolicymanagementService, private fb: FormBuilder,
     private router: Router) { 
    this.editPrivacypDataForm = this.fb.group({
      privacy_policy_data: new FormControl('', Validators.required),
     
    })
  }

  ngOnInit() {
    this.privacyp_id = this.actRoute.snapshot.params.id
    console.log(this.privacyp_id, "privacypolicy")
    this.fetchPrivacypDataById()
  }

  fetchPrivacypDataById(){
    this.privacyPService.fetchPrivacyPolicyById(this.privacyp_id).subscribe((data:any)=>{
      this.fetchprivacyDataArray = data.response.privacy_policy_data
      console.log(this.fetchprivacyDataArray)
      this.editPrivacypDataForm.patchValue({
        privacy_policy_data  : this.fetchprivacyDataArray
      })
    },(err=>{
      console.log(err)
    }))
  }

  get f() { return this.editPrivacypDataForm.controls}

  onSubmit(){
    this.submitted = true;
    this.privacyPService.updatePrivacyPloicy(this.editPrivacypDataForm.value).subscribe((data)=>{
      console.log(data, "update help data")
      this.editPrivacypDataForm.reset()
      this.router.navigate(['/side-menu',{outlets:{sidebar:'privacypolicy'}}], {relativeTo: this.actRoute})
    }, (err=>{
      console.log(err)
    }))
  }
}
