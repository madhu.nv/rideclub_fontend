import { Component, OnInit } from '@angular/core';
import { PrivacypolicymanagementService } from 'src/app/Services/privacypolicymanagement/privacypolicymanagement.service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {
  fetchPrivacypArray: any;
  id:any;
  constructor( private privacypService: PrivacypolicymanagementService) { }

  ngOnInit() {
    this.fetchprivacyData()
  }

  fetchprivacyData(){
    this.privacypService.fetchprivacyPolicy().subscribe((data: any)=>{
      console.log(data)
      this.fetchPrivacypArray = data.response[0].privacy_policy_data
      this.id = data.response[0]._id
      console.log(this.id)
      console.log(this.fetchPrivacypArray, "privacy data")
    }, (err)=>{
      console.log(err)
    })
  }
}
