import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivacyMangementRoutingModule } from './privacymanagement-routing.module';
import { PrivacyComponent } from './privacy/privacy.component';
import { UpdatePrivacyPolicyDataComponent } from './update-privacy-policy-data/update-privacy-policy-data.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ViewPrivacyPolicyComponent } from './view-privacy-policy/view-privacy-policy.component';
import { AddPrivacyComponent } from './add-privacy/add-privacy.component';




@NgModule({
  declarations: [PrivacyComponent, UpdatePrivacyPolicyDataComponent, ViewPrivacyPolicyComponent, AddPrivacyComponent],
  imports: [
    CommonModule,
    PrivacyMangementRoutingModule,
    FormsModule ,
    ReactiveFormsModule,
    CKEditorModule
  ]
})
export class PrivacyManagementModule { }
