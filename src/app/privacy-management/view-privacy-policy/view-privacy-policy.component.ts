import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PrivacypolicymanagementService } from 'src/app/Services/privacypolicymanagement/privacypolicymanagement.service';

@Component({
  selector: 'app-view-privacy-policy',
  templateUrl: './view-privacy-policy.component.html',
  styleUrls: ['./view-privacy-policy.component.css']
})
export class ViewPrivacyPolicyComponent implements OnInit {
  privacyp_id:any;
  fetchPrivacypArray: any;
  constructor(private policypDataService: PrivacypolicymanagementService, private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.privacyp_id = this.actRoute.snapshot.params.id
    console.log(this.privacyp_id, "privacyp")
    this.fetchPrivacyData()
  }

  fetchPrivacyData(){
    this.policypDataService.fetchPrivacyPolicyById(this.privacyp_id).subscribe((data:any)=>{
      console.log(data)
      this.fetchPrivacypArray = data.response. privacy_policy_data
    })
  }

}
