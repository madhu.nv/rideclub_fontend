import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrivacyComponent } from './privacy/privacy.component';
import { UpdatePrivacyPolicyDataComponent } from './update-privacy-policy-data/update-privacy-policy-data.component';
import { ViewPrivacyPolicyComponent } from './view-privacy-policy/view-privacy-policy.component';
import { AddPrivacyComponent } from './add-privacy/add-privacy.component';

const routes: Routes = [
    { path: "", component:PrivacyComponent},
    { path: "update_privacyp/:id", component:UpdatePrivacyPolicyDataComponent},
    { path: "view_privacyp/:id", component:ViewPrivacyPolicyComponent},
    { path: "add_privacy", component: AddPrivacyComponent}
   
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivacyMangementRoutingModule { }