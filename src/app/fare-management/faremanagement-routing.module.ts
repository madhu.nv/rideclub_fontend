import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FareComponent } from './fare/fare.component';
import { AddFareComponent } from './add-fare/add-fare.component';
import { EditFareComponent } from './edit-fare/edit-fare.component';
import { ViewFareComponent } from './view-fare/view-fare.component';


const routes: Routes = [
      { path: '', component: FareComponent} ,
      { path: 'add_fare', component: AddFareComponent},
      { path: 'edit_fare/:id', component: EditFareComponent},
      { path: 'view_fare/:id', component: ViewFareComponent} 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FareMangementRoutingModule {}
