import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FareMangementRoutingModule } from './FareManagement-routing.module';
import { FareComponent } from './fare/fare.component';
import { AddFareComponent } from './add-fare/add-fare.component';
import { EditFareComponent } from './edit-fare/edit-fare.component';
import { ViewFareComponent } from './view-fare/view-fare.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';



@NgModule({
  declarations: [FareComponent, AddFareComponent, EditFareComponent, ViewFareComponent],
  imports: [
    CommonModule,
    FareMangementRoutingModule,
    FormsModule ,
    ReactiveFormsModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
  ]
})
export class FareManagementModule { }
