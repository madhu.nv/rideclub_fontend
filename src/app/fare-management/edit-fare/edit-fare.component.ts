import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { LocationManagementService } from 'src/app/Services/location-management/location-management.service';
import { FaremanagementService } from 'src/app/Services/fareManagement/faremanagement.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-fare',
  templateUrl: './edit-fare.component.html',
  styleUrls: ['./edit-fare.component.css']
})
export class EditFareComponent implements OnInit {
  id : String
  countries: any;
  selectedLevel;
  states: any;
  stateid;
  cities: any;
  country;
  state;
  city;
  petrolCharge;
  dieselCharge;
  cancellacharge;
  perKmCharge;
  id1;
  numberPattern = "^((\\+91-?)|0)?[0-9]$";
  submitted = false;
  updateFareForm: FormGroup = new FormGroup({})
  constructor(private actRoute: ActivatedRoute,private router: Router,private locationService: LocationManagementService,private toastr: ToastrService,  private fb: FormBuilder, private fareService: FaremanagementService) { 
    this.updateFareForm = this.fb.group({
      // country: new FormControl('', Validators.required),
      // state: new FormControl('', Validators.required),
      // city: new FormControl(''),
      petrolCharge: new FormControl('', [Validators.required, Validators.pattern(this.numberPattern)]),
      dieselCharge: new FormControl('', [Validators.required, Validators.pattern(this.numberPattern)]),
      // cancellationCharge: new FormControl('', [Validators.required, Validators.pattern(this.numberPattern)]),
      perKmCharge: new FormControl('', [Validators.required, Validators.pattern(this.numberPattern)]),
      id: new FormControl('')
     })
  }

  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "id");
    this.getCountry();
    this.fetchData()
  }
  getCountry(){
    this.locationService.fetchCountry().subscribe((data: any)=>{
      console.log(data);
      this.countries = data.data
    })
  }
  getState(){
    this.locationService.fetchSingleState(this.selectedLevel).subscribe((data:any)=>{
      console.log(data)
      this.states = data.data
    })
  }
  
  selected(){
    console.log(this.selectedLevel);
    this.getState()
  }

  getStateCity(){
    this.locationService.fetchCityById(this.stateid).subscribe((data:any)=>{
      console.log(data, "stateCoity");
      this.cities = data.data
    })
  }
  stateSelected(){
    console.log(this.stateid)
    this.getStateCity()
  }

  fetchData(){
    this.fareService.fetchFareById(this.id).subscribe((data:any)=>{
      console.log(data);
      // this.country = data.response.country,
      // this.state = data.response.state,
      // this.city = data.response.city,
      this.petrolCharge = data.response.petrolCharge,
      this.dieselCharge = data.response.dieselCharge,
      this.cancellacharge =  data.response.cancellationCharge,
      this.perKmCharge = data.response.perKmCharge
      this.id1 = data.response._id
      this.updateFareForm.patchValue({
       
        petrolCharge: this.petrolCharge,
        dieselCharge: this.dieselCharge,
        // cancellationCharge: this.cancellacharge,
        perKmCharge: this.perKmCharge,
        id: this.id1
      })
    },(err=>{
      console.log(err)
    }))
   
  }
  get f() { return this.updateFareForm.controls; }
  onSubmit(){
    this.submitted = true;
   this.fareService.updateFare(this.updateFareForm.value).subscribe((data)=>{
     console.log(data)
     this.updateFareForm.reset();
     this.toastr.success("updated Successfully")
     this.router.navigate(['/side-menu',{outlets:{sidebar:'fare'}}], {relativeTo: this.actRoute})
   },(err=>{
     console.log(err)
   }))
  }


}
