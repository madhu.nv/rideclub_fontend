import { Component, OnInit } from '@angular/core';
import { LocationManagementService } from 'src/app/Services/location-management/location-management.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { FaremanagementService } from 'src/app/Services/fareManagement/faremanagement.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-fare',
  templateUrl: './add-fare.component.html',
  styleUrls: ['./add-fare.component.css']
})
export class AddFareComponent implements OnInit {
  countries: any;
  states: any;
  selectedLevel;
  stateid;
  cities: any;
  submitted = false;
  numberPattern = "^[0-9]*$"; 
  fareForm: FormGroup = new FormGroup({})
  constructor( private toastr: ToastrService,private route: ActivatedRoute,private router: Router,private locationService: LocationManagementService, private fareService: FaremanagementService, private fb: FormBuilder) { 
    this.fareForm = this.fb.group({
      country: new FormControl('', Validators.required),
      state: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      petrolCharge: new FormControl('', [Validators.required, Validators.pattern(this.numberPattern)]),
      dieselCharge: new FormControl('', [Validators.required, Validators.pattern(this.numberPattern)]),
      // cancellationCharge: new FormControl('', [Validators.required, Validators.pattern(this.numberPattern)]),
      perKmCharge: new FormControl('', [Validators.required, Validators.pattern(this.numberPattern)])
     })
  }

  ngOnInit() {
    this.getCountry()
  }
getCountry(){
  this.locationService.fetchCountry().subscribe((data: any)=>{
    console.log(data);
    this.countries = data.data
  })
}

getState(){
  this.locationService.fetchSingleState(this.selectedLevel).subscribe((data:any)=>{
    console.log(data)
    this.states = data.data
  })
}

selected(){
  console.log(this.selectedLevel);
  this.getState()
}


getStateCity(){
  this.locationService.fetchCityById(this.stateid).subscribe((data:any)=>{
    console.log(data, "stateCoity");
    this.cities = data.data
  })
}
stateSelected(){
  console.log(this.stateid)
  this.getStateCity()
}

get f() { return this.fareForm.controls; }

onSubmit(){
  this.submitted = true;
  console.log(this.fareForm.value)
  this.fareService.createFare(this.fareForm.value).subscribe((data)=>{
    console.log(data);
    this.toastr.success("Submitted Successfully")
    this.fareForm.reset()
    this.router.navigate(['/side-menu',{outlets:{sidebar:'fare'}}], {relativeTo: this.route})
  },(err=>{
    console.log(err);
    this.toastr.error(`${err.error.message}`)
  }))
}


}
