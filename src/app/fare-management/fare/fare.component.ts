import { Component, OnInit } from '@angular/core';
import { ExcelServicesService } from 'src/app/Services/excel/excel-services.service';
import { FaremanagementService } from 'src/app/Services/fareManagement/faremanagement.service';

@Component({
  selector: 'app-fare',
  templateUrl: './fare.component.html',
  styleUrls: ['./fare.component.css']
})
export class FareComponent implements OnInit {
FareArray: any;
p=1;
searchText;
excel = []
obj = {}
  constructor( private fareService: FaremanagementService, private excelService:ExcelServicesService) { }

  ngOnInit() {
    this.fetchFareData()
  }
  fetchFareData(){
    this.fareService.fetchFare().subscribe((data: any)=>{
      console.log(data)
      this.FareArray = data.response
    })
  }
  delete(fare){
    console.log(fare);
    var r = confirm("Do you really want to delete this record?");
    if(r==true){
      this.fareService.deleteFare(fare._id).subscribe((data)=>{
        console.log(data);
        this.fetchFareData();
      })
    }else{
      console.log("error")
    }
  }
  exportAsXLSX():void {  
    this.FareArray.forEach(element => {
    this.obj = {
      Country: element.country,
      State: element.state,
      City: element.city ,
      PetrolCharge: element.petrolCharge,
      DieselCharge: element.dieselCharge,
      CancellationCharge: element.cancellationCharge,
      FixedCharge: element.fixedCharge,
    }
        this.excel.push(this.obj)
      
    });
      this.excelService.exportAsExcelFile(this.excel, 'Fare');  
   }
}
