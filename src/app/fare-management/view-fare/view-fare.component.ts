import { Component, OnInit } from '@angular/core';
import { FaremanagementService } from 'src/app/Services/fareManagement/faremanagement.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-fare',
  templateUrl: './view-fare.component.html',
  styleUrls: ['./view-fare.component.css']
})
export class ViewFareComponent implements OnInit {
  id : String
  country;
  state;
  city;
  petrolCharge;
  dieselCharge;
  // cancellacharge;
  perKmCharge;
  constructor(private fareService: FaremanagementService, private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "id");
    this.fetchData();
  }
  fetchData(){
    this.fareService.fetchFareById(this.id).subscribe((data:any)=>{
      console.log(data);
      this.country = data.response.country,
      this.state = data.response.state,
      this.city = data.response.city,
      this.petrolCharge = data.response.petrolCharge,
      this.dieselCharge = data.response.dieselCharge,
      // this.cancellacharge =  data.response.cancellationCharge,
      this.perKmCharge = data.response.perKmCharge
    },(err=>{
      console.log(err)
    }))
  }
}
