import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { identifierModuleUrl } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class HelpManagementService {
helpUrl = "http://rider-club.dxminds.online:5555/api/help"
// helpUrl = "http://localhost:5555/api/help"
  constructor( private http: HttpClient) {}
createHelp(data){
  return this.http.post(this.helpUrl + '/create', data)
}
  fetchHelp(){
    return this.http.get(this.helpUrl + '/fetch')
  }

  fetchById(id){
    console.log(id, "service id")
    return this.http.get(this.helpUrl + '/fetchData/' + id) 
  }

  updateHelp(data){
    return this.http.post(this.helpUrl + '/update/' + data.id, data)
  }
}
