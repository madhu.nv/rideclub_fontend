import { TestBed } from '@angular/core/testing';

import { HelpManagementService } from './help-management.service';

describe('HelpManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HelpManagementService = TestBed.get(HelpManagementService);
    expect(service).toBeTruthy();
  });
});
