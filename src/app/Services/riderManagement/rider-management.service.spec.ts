import { TestBed } from '@angular/core/testing';

import { RiderManagementService } from './rider-management.service';

describe('RiderManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RiderManagementService = TestBed.get(RiderManagementService);
    expect(service).toBeTruthy();
  });
});
