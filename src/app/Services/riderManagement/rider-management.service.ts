import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RiderManagementService {
riderUrl = "http://rider-club.dxminds.online:5555/api/rider"
approvalRiderUrl = "http://rider-club.dxminds.online:5555/api/user"
// riderUrl = "http://localhost:5555/api/rider"
// approvalRiderUrl = "http://localhost:5555/api/user"
  constructor(private http: HttpClient) { }
  fetchRider(){
    return this.http.get(this.riderUrl + '/fetchRider');
  }

  // fetchVehicleInfo(uid){
  //   console.log(uid)
  //   return this.http.get(this.riderUrl + '/getVehicleInfo/' + uid)
  // }

  fetchRiderKyc(rider){
    console.log(rider)
    return this.http.get(this.riderUrl + '/getKycriderDetail/' + rider)
  }

  RiderApprovalReject(approvalRider){
    console.log(approvalRider, "service")
    return this.http.put(this.approvalRiderUrl + '/check/verifyingstatus',approvalRider)
  }

  postRiderDl(dlApproval){
    console.log(dlApproval, "reject service")
    return this.http.put(this.approvalRiderUrl + '/check/verifyingstatus',dlApproval )
  }

  deleteRider(uid){
    console.log(uid, "service Uid")
    return this.http.delete(this.riderUrl + '/deleterider/' + uid)
  
  }
  getRiderSingleData(id){
    return this.http.get(this.riderUrl + '/getsingleData/' +id)
  }
}
