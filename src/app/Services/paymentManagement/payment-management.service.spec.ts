import { TestBed } from '@angular/core/testing';

import { PaymentManagementService } from './payment-management.service';

describe('PaymentManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentManagementService = TestBed.get(PaymentManagementService);
    expect(service).toBeTruthy();
  });
});
