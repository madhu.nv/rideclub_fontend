import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PaymentManagementService {
  razorPayUrl = "https://api.razorpay.com/v1/payouts";
  constructor(private http: HttpClient) { }

  razorPayOrder(data) {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Access-Control-Allow-Headers', 'Content-Type')
      .append('Access-Control-Allow-Methods', 'GET')
      .append('Access-Control-Allow-Origin', '*');
    return this.http.post(this.razorPayUrl, data, {headers: headers})
  }
}
