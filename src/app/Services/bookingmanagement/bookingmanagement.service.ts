import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookingmanagementService {
  bookingUrl = "http://rider-club.dxminds.online:5555/api/booking"
// bookingUrl = "http://localhost:5555/api/booking"
  constructor( private http: HttpClient) { }
  fetchBookingData(){
    return this.http.get(this.bookingUrl + '/currentfindride')
  }

  fetchAllDetails(id){
    return this.http.get(this.bookingUrl + '/fetchdetails/' + id)
  }

  // ScheduleRide
  fetchSchedule(){
    return this.http.get(this.bookingUrl + '/schedulefindride')
  }

  fetchScheRecurrData(id){
    return this.http.get(this.bookingUrl + '/fetchScheduleDetails/' + id)
  }

  //Current offer ride
  fetchCurrentOfferRide(){
    return this.http.get(this.bookingUrl + '/currentofferride')
  }

  fetchcurOfrDetails(id){
    return this.http.get(this.bookingUrl + '/curPassengerData/' +id)
  }

  //Schedule/recurr offer ride
  fetchSechOfferRide(){
    return this.http.get(this.bookingUrl + '/scheduleOfferRide')
  }

  fetchScheOfferRideDetails(id){
    return this.http.get(this.bookingUrl + '/scheduleofferridedetails/' + id)
  }
}
