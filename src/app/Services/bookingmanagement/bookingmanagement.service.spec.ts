import { TestBed } from '@angular/core/testing';

import { BookingmanagementService } from './bookingmanagement.service';

describe('BookingmanagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BookingmanagementService = TestBed.get(BookingmanagementService);
    expect(service).toBeTruthy();
  });
});
