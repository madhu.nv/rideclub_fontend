import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotificationManagementService {
// NotifUrl = "http://localhost:5555/api/notif"
NotifUrl = "http://rider-club.dxminds.online:5555/api/notif"
  constructor(private http: HttpClient) { }
  createNotif(data){
    return this.http.post(this.NotifUrl + '/create/notif', data)
  }
  fetchNotif(){
    return this.http.get(this.NotifUrl + '/fetch/notif')
  }
  updateNotif(data){
    return this.http.post(this.NotifUrl + '/update/notif', data)
  }

  // POp up
  craetePopup(data){
    return this.http.post(this.NotifUrl + '/create/popup', data)
  }
  fetchPopUp(){
    return this.http.get(this.NotifUrl + '/fetch/popup');
  }
  updatePopUp(data){
    return this.http.post(this.NotifUrl + '/update/popup', data)
  }
}
