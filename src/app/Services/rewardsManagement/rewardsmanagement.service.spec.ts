import { TestBed } from '@angular/core/testing';

import { RewardsmanagementService } from './rewardsmanagement.service';

describe('RewardsmanagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RewardsmanagementService = TestBed.get(RewardsmanagementService);
    expect(service).toBeTruthy();
  });
});
