import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RewardsmanagementService {
// rewardsUrl = "http://localhost:5555/api/rewards"
rewardsUrl = "http://rider-club.dxminds.online:5555/api/rewards"
  constructor(private http: HttpClient) { }
  createRewards(data){
   return this.http.post(this.rewardsUrl + '/rewards/create', data)
  }
  fetchRewards(){
    return this.http.get(this.rewardsUrl + '/rewards/fetch');
  }
  fetchSingleRewards(id){
    return this.http.get(this.rewardsUrl + '/fetchrewards/' + id);
  }
  updateRewards(data){
    return this.http.post( this.rewardsUrl + '/update/rewards', data)
  }
  deleteRewards(id){
    return this.http.delete(this.rewardsUrl + '/delete/rewards/' + id)
  }

  //Bike Rewards
  createBikeRewards(data){
    return this.http.post(this.rewardsUrl + '/bikerewards/create', data)
  }
  fetchBikeRewards(){
    return this.http.get(this.rewardsUrl + '/bikerewards/fetch');
  }
  fetchSingleBikeRewards(id){
    return this.http.get(this.rewardsUrl + '/fetchbikerewards/' + id);
  }
  updateBikeRewards(data){
    return this.http.post( this.rewardsUrl + '/update/bikerewards', data)
  }
  deleteBikeRewards(id){
    return this.http.delete(this.rewardsUrl + '/delete/bikerewards/' + id)
  }

  //Trveller Rewards
  createTravellerRewards(data){
    return this.http.post(this.rewardsUrl + '/travellerrewards/create', data)
  }
  fetchTravellerRewards(){
    return this.http.get(this.rewardsUrl + '/travellerbikerewards/fetch');
  }
  fetchSingleTravellerRewards(id){
    return this.http.get(this.rewardsUrl + '/fetchtravellerrewards/' + id);
  }
  updateTravellerRewards(data){
    return this.http.post( this.rewardsUrl + '/update/travellerrewards', data)
  }
  deleteTravellerRewards(id){
    return this.http.delete(this.rewardsUrl + '/delete/travellerrewards/' + id)
  }
}
