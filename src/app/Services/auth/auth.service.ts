import { Injectable, NgZone } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFireAuth } from '@angular/fire/auth';
// import {FirebaseAuthState} from 'angularfire2'
import { BehaviorSubject, Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { AngularFireDatabase } from '@angular/fire/database';
import { auth } from 'firebase';
import { FirebaseAuth } from '@angular/fire';
import { User } from "../user";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
// import { auth } from  'firebase/app';
// import { AngularFireAuth } from "@angular/fire/auth";
// import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: any;
  // private loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || ('false'));
  // isLoggedIn = false;
  // redirectUrl: string;
  // isLoggedIn = new BehaviorSubject<boolean>(false);
  // helpUrl = "http://rider-club.dxminds.online:5555/api/help"
signInUrl = "http://localhost:5555"
  resultData: Object;
  constructor(public afAuth: AngularFireAuth,
    public afs: AngularFirestore,
    public router: Router,
    private toastr: ToastrService, public ngZone: NgZone,private http: HttpClient) {

    this.afAuth.authState.subscribe((user) => {
      console.log(user, "user")
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'))
      }
    })
  }

  // Returns true when user is looged in and email is verified
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    console.log(user, "mmmmmm")
    return (user !== null) ? true : false;
  }

  /* Sign up */
  // SignUp(email: string, password: string) {
  //   return this.afAuth
  //     .auth
  //     .createUserWithEmailAndPassword(email, password)
  // }

  SignUp(email: string, password: string) {
    return this.http.post(this.signInUrl + '/api/user/register', {email, password})
  }
  /* Sign in */
  SignIn(email: string, password: string) {
    return this.afAuth
      .auth
      .signInWithEmailAndPassword(email, password)

    // .then((result)=>{
    //   console.log(result,"result" )
    //   this.isLoggedIn = true;
    //   // let user1 = result.user
    //   // localStorage.setItem('testObject', JSON.stringify(user1));
    //   // this.SetUserData(result.user);
    // })
  }
  /* Sign out */
  SignIn1(username,password){
    return this.http.post(this.signInUrl + '/api/user/login', {username: username, password: password})
      .pipe(
        map(result => {
          console.log(result)
          this.resultData = result;
          // console.log(this.resultData.data.token)
          // localStorage.setItem('access_token', this.resultData.data.token);
          // var datanew = localStorage.getItem('access_token')
          return true;
        })
      );
  }
  SignOut() {
    // this.afAuth
    //   .auth
    //   .signOut()
    return this.http.post(this.signInUrl + '/api/user/logout', {})



  }


  // / Reset Forggot password
  ForgotPassword(email: string) {
    return this.afAuth.auth.sendPasswordResetEmail(email)
  }

  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
      .then(() => {
        this.router.navigate(['verify-email']);
      })
  }



}
