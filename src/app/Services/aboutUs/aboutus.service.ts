import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AboutusService {
  baseUrl="http://rider-club.dxminds.online:5555/api/aboutus"
  // baseUrl = environment.url + '/api/aboutus'
  // baseUrl="http://localhost:5555/api/aboutus"
  constructor(private http: HttpClient) {}
 getData(){
   return this.http.get(`${this.baseUrl}/fetch`)
 }
 postData(data){
   return this.http.post(`${this.baseUrl}/createAboutUs`, data)
 }
 updateAboutUs(aboutUsData){
console.log(aboutUsData);
return this.http.post(this.baseUrl + '/update', aboutUsData)
 }
 deleteAboutUs(id){
   console.log(id, "ffjfjf")
   return this.http.delete(this.baseUrl + '/delete/' + id)
 }
 getDataById(id){
   return this.http.get( this.baseUrl + '/fetchData/' + id)
 }

}
