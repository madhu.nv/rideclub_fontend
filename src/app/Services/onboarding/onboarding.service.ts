import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class OnboardingService {
  onboardingUrl ="http://rider-club.dxminds.online:5555/api/onboarding"
//  onboardingUrl = environment.url + '/api/onboarding'
// onboardingUrl = "http://localhost:5555/api/onboarding"
  constructor( private http: HttpClient) { }

createOnboarding(form){
return this.http.post(this.onboardingUrl + '/create',form)
}
  fetchOnboarding(){
    return this.http.get(this.onboardingUrl + '/find')
  }
updateOnboarding(updateData){

  console.log("update service data", updateData)
  return this.http.post(this.onboardingUrl + '/update', updateData)
}
  deleteOnboarding(data){
    console.log(data._id, "service data");
    return this.http.delete(this.onboardingUrl + '/delete/' + data._id)
  }
  fetchOnboardingById(_id){
    console.log(_id)
    return this.http.get(this.onboardingUrl + '/find/'+ _id)
  }
}
