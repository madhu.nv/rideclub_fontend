import { TestBed } from '@angular/core/testing';

import { PrivacypolicymanagementService } from './privacypolicymanagement.service';

describe('PrivacypolicymanagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrivacypolicymanagementService = TestBed.get(PrivacypolicymanagementService);
    expect(service).toBeTruthy();
  });
});
