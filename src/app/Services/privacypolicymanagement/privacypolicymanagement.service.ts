import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PrivacypolicymanagementService {
  privacypUrl = "http://rider-club.dxminds.online:5555/api/privacyandpolicy"
  // privacypUrl = "http://localhost:5555/api/privacyandpolicy"
  constructor(private http: HttpClient) { }
  craetePrivacy(data){
    return this.http.post(this.privacypUrl + '/create', data)
  }
  fetchprivacyPolicy(){
    return this.http.get(this.privacypUrl + '/find')
  }

fetchPrivacyPolicyById(id){
  return this.http.get(this.privacypUrl + '/fetchprivacy/' + id)
}

  updatePrivacyPloicy(data){
    return this.http.post(this.privacypUrl + '/update/' + data.id, data)
  }
}
