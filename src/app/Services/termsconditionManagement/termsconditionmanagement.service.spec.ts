import { TestBed } from '@angular/core/testing';

import { TermsconditionmanagementService } from './termsconditionmanagement.service';

describe('TermsconditionmanagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TermsconditionmanagementService = TestBed.get(TermsconditionmanagementService);
    expect(service).toBeTruthy();
  });
});
