import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TermsconditionmanagementService {
  // termsCUrl = "http://localhost:5555/api/termsAndCondition"
  termsCUrl = "http://rider-club.dxminds.online:5555/api/termsAndCondition"
  constructor(private http: HttpClient) {}


  fetchTermsCondition(){
    return this.http.get(this.termsCUrl + '/fetchtermsandconditions')
  }

  
  fetchTermscById(id){
    console.log(id, "service id")
    return this.http.get(this.termsCUrl + '/fetchtermsC/' + id) 
  }

  updateTermsCondition(data){
    return this.http.post(this.termsCUrl + '/updatetermscondition/' + data.id, data)
  }
}
