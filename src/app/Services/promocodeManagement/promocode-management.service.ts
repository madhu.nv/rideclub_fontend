import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PromocodeManagementService {
// promocodeUrl = "http://localhost:5555/api/promocode"
promocodeUrl = "http://rider-club.dxminds.online:5555/api/promocode"
  constructor(private http: HttpClient) { }

  createPromoCode(data){
    return this.http.post( this.promocodeUrl + '/create/promocode', data)
  }
  fetchPromocode(){
    return this.http.get( this.promocodeUrl + '/fetch/promocode')
  }
  fetchSinlePromocode(id){
    return this.http.get( this.promocodeUrl + '/fetchsinglecode/promocode/' + id)
  }
  updatePromoCode(data){
    return this.http.post( this.promocodeUrl + '/update/promocode', data)
  }
  deletePromocode(id){
    return this.http.delete( this.promocodeUrl + '/delete/promocode/' + id)
  }
}
