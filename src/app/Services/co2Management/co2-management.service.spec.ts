import { TestBed } from '@angular/core/testing';

import { Co2ManagementService } from './co2-management.service';

describe('Co2ManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Co2ManagementService = TestBed.get(Co2ManagementService);
    expect(service).toBeTruthy();
  });
});
