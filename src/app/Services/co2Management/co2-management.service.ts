import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Co2ManagementService {
  co2Url = "http://rider-club.dxminds.online:5555/api/co2"
// co2Url = "http://localhost:5555/api/co2"
  constructor( private http: HttpClient) { }

  createCarNorms(data){
    return this.http.post(this.co2Url + '/create/', data)
  }

  fetchCarNorms(){
    return this.http.get(this.co2Url + '/fetch')
  }

  updateCarNorms(data){
    return this.http.post(this.co2Url + '/update', data)
  }

  deleteCarNorms(id){
    return this.http.delete(this.co2Url + '/delete/' + id)
  }

  //Bike Norms

  craeteBikeNorms(data) {
    return this.http.post(this.co2Url + '/createBikeNorms', data)
  }

  fetchBikeNorms(){
    return this.http.get(this.co2Url + '/fetchBikeNorms')
  }

  updateBikeNorms(data){
    return this.http.post(this.co2Url + '/updateBikeNorms', data)
  }

  deleteBikeNorms(id) {
    return this.http.delete(this.co2Url + '/deleteBikeNorms/' + id)
  }
}
