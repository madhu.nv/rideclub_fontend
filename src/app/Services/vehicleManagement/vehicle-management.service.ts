import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VehicleManagementService {
  url = "http://rider-club.dxminds.online:5555/api/vehicleInfo"
  
  constructor(private http: HttpClient) {}
  
createVehicleType( data){
  console.log(data, "service");
  return this.http.post(this.url + '/create/vehicleData', data)
}
getImage(){
  return this.http.get(this.url + '/fetchvehicleData')
}

getVehicleData(id){
  return this.http.get(this.url + '/fetchallData/' + id)
}

updateData( data1){
  // console.log(data, "id")
  return this.http.post(this.url + '/updateData' , data1)
}

deleteData(id){
  return this.http.delete(this.url + '/deleteData/' + id)
}

}
