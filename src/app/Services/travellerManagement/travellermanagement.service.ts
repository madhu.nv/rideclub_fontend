import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class TravellermanagementService {
  url = "http://rider-club.dxminds.online:5555/api/traveller"
// url = "http://localhost:5555/api/traveller"
// approvalUrl = "http://localhost:5555/api/user"
approvalUrl = "http://rider-club.dxminds.online:5555/api/user"
  constructor( private http: HttpClient, private firestore: AngularFirestore) { }
  getTraveller(){
    return this.http.get(`${this.url}/fetchTraveller`)
  }
  getKyc(uid){
    console.log(uid, "service")
    return this.http.get(this.url + '/getKycDetail/' + uid)
  }
  postApproval(approval){
    console.log(approval, "service")
    return this.http.put(this.approvalUrl + '/check/verifyingstatus',approval )
  }
  postReject(reject){
    console.log(reject, "reject service")
    return this.http.put(this.approvalUrl + '/check/verifyingstatus',reject )
  }
deleteTraveller(traveller){
  return this.http.delete(this.url + '/deletetraveller/' + traveller)
}
getSingleTraveller(id){
  return this.http.get(this.url + '/fetchSingleData/' +id)
}
}
