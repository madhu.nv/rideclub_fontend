import { TestBed } from '@angular/core/testing';

import { TravellermanagementService } from './travellermanagement.service';

describe('TravellermanagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TravellermanagementService = TestBed.get(TravellermanagementService);
    expect(service).toBeTruthy();
  });
});
