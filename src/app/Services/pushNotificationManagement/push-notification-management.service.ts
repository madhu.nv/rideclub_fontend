import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PushNotificationManagementService {
  // pNotifUrl  = "http://rider-club.dxminds.online:5555/api/pnotification"
pNotifUrl = "http://localhost:5555/api/pnotification"
  constructor(private http: HttpClient) { }

 createNotif(data){
   return this.http.post(this.pNotifUrl + '/sendNotification', data)
 }

 fetchNotif(){
   return this.http.get(this.pNotifUrl + '/fetchNotification')
 }
}
