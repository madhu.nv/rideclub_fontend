import { TestBed } from '@angular/core/testing';

import { PushNotificationManagementService } from './push-notification-management.service';

describe('PushNotificationManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PushNotificationManagementService = TestBed.get(PushNotificationManagementService);
    expect(service).toBeTruthy();
  });
});
