import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BillVerificationService {
  billUrl =  "http://rider-club.dxminds.online:5555/api/usrrewardsBill"
  // billUrl = "http://localhost:5555/api/usrrewardsBill"
  constructor( private http: HttpClient) { }

  fetchUserData(){
    return this.http.get( this.billUrl + '/getUserData')
  }

  fetchSingleDocument(uid){
    return this.http.get(this.billUrl + '/getSingleData/' + uid)
  }

  fetchPendingDocument(){
    return this.http.get(this.billUrl + '/fetchpendingData')
  }

  fetchSinglependingData(id){
    return this.http.get(this.billUrl + '/fetchPendingsingleDocument/' + id)
  }

  updatependingDocument(data){
    return this.http.post(this.billUrl + '/updateStatus', data)
  }
}
