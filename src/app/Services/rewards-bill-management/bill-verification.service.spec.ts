import { TestBed } from '@angular/core/testing';

import { BillVerificationService } from './bill-verification.service';

describe('BillVerificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BillVerificationService = TestBed.get(BillVerificationService);
    expect(service).toBeTruthy();
  });
});
