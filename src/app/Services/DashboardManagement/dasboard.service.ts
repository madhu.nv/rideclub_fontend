import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DasboardService {
  dashboardUrl = "http://rider-club.dxminds.online:5555/api/dashboard"
// dashboardUrl = "http://localhost:5555/api/dashboard"
  constructor(private http: HttpClient) { }
  fetchData(){
    return this.http.get(this.dashboardUrl + '/fetch')
  }
}
