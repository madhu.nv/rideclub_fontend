import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { database } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class LocationManagementService {
// locationUrl = "http://localhost:5555/api/location"
locationUrl = "http://rider-club.dxminds.online:5555/api/location"
  constructor(private http: HttpClient) { }
  createCountry(data){
    return this.http.post(this.locationUrl + '/create/country' , data)
  }
  fetchCountry(){
    return this.http.get(this.locationUrl + '/fetch/country')
  }
  createState(data){
    return this.http.post(this.locationUrl + '/create/state/' + data.country, data )
  }
  fetchState(){
    return this.http.get(this.locationUrl + '/fetch/state')
  }
 
  fetchSingleState(id){
    return this.http.get(this.locationUrl + '/fetch/state/data/' +id)
  }
  createCity(data){
    return this.http.post(this.locationUrl + '/create/city/' + data.state, data)
  }
  fetchCity(){
    return this.http.get(this.locationUrl + '/fetch/city')
  }
  fetchCityById(id){
    return this.http.get(this.locationUrl + '/fetch/StateCity/' + id)
  }
  updateCountry(data){
    console.log(data, "service")
    return this.http.post(this.locationUrl + '/update/country' , data)
  }
  updateState(data){
    return this.http.post(this.locationUrl + '/update/state', data)
  }
  updateCity(data){
    return this.http.post(this.locationUrl + '/update/city', data)
  }
  deleteCountry(id){
    return this.http.delete(this.locationUrl + '/delete/country/' + id)
  }
  deleteState(id){
    return this.http.delete(this.locationUrl + '/delete/state/' + id)
  }
  deleteCity(id){
    return this.http.delete(this.locationUrl + '/delete/City/' + id)
  }
}
