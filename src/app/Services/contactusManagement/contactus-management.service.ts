import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactusManagementService {
// contactUsUrl = "http://localhost:5555/api/contactus"
contactUsUrl = "http://rider-club.dxminds.online:5555/api/contactus"
  constructor(private http: HttpClient) { }
  fetchContactUs(){
    return this.http.get(this.contactUsUrl + '/fetch/contactus')
  }
  updateContactUs(data){
    return this.http.post(this.contactUsUrl + '/update/contactus', data)
  }
}
