import { TestBed } from '@angular/core/testing';

import { ContactusManagementService } from './contactus-management.service';

describe('ContactusManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContactusManagementService = TestBed.get(ContactusManagementService);
    expect(service).toBeTruthy();
  });
});
