import { TestBed } from '@angular/core/testing';

import { FaremanagementService } from './faremanagement.service';

describe('FaremanagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FaremanagementService = TestBed.get(FaremanagementService);
    expect(service).toBeTruthy();
  });
});
