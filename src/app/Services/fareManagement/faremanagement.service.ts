import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FaremanagementService {
fareUrl = "http://rider-club.dxminds.online:5555/api/fare"
// fareUrl = "http://localhost:5555/api/fare"
  constructor(private http: HttpClient) {}

  createFare(data){
    return this.http.post(this.fareUrl + '/create/fare' , data)
  }

  fetchFare(){
    return this.http.get(this.fareUrl + '/fetch/fare')
  }

  fetchFareById(id){
    return this.http.get(this.fareUrl + '/fetchdata/' + id)
  }
 
  updateFare(data){
    return this.http.post(this.fareUrl + '/update/fare', data)
  }

  deleteFare(id){
    return this.http.delete(this.fareUrl + '/delete/fare/' +id)
  }
}
