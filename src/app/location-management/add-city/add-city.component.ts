import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { LocationManagementService } from 'src/app/Services/location-management/location-management.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.css']
})
export class AddCityComponent implements OnInit {
  addCityForm: FormGroup = new FormGroup({})
  countries:any;
  states: any;
  selectedLevel;
  id;
  submitted = false;
 
  constructor(private locationService: LocationManagementService,private route: ActivatedRoute, private fb: FormBuilder, private toastr: ToastrService, private router: Router) { 
    this.addCityForm = this.fb.group({
      country: new FormControl('', Validators.required),
      state: new FormControl('', Validators.required),
      name: new FormControl(''),
      status: new FormControl(''),
     })
  }

  ngOnInit() {
    this.getCountry();
   
  }
 //Get countries list
 getCountry(){
  this.locationService.fetchCountry().subscribe((data:any)=>{
    console.log(data);
    this.countries = data.data
  })
}

selected(){
  console.log(this.selectedLevel)
  this.id = this.selectedLevel;
  this.getState();
}

getState(){
  console.log(this.id, "mmmmm")
  this.locationService.fetchSingleState(this.id).subscribe((data:any)=>{
    console.log(data)
    this.states = data.data
  })
}
get fval() {
  return this.addCityForm.controls;
  }
onSubmit(){
  
  this.submitted = true;
  console.log(this.addCityForm.value)
  this.locationService.createCity(this.addCityForm.value).subscribe((data)=>{
    console.log(data, "city Data")
    this.router.navigate(['/side-menu',{outlets:{sidebar:'location'}}], {relativeTo: this.route})
  },(err=>{
    this.toastr.error(`${err.error.message}`)
  }))
}
}
