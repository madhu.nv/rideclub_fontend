import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocationComponent } from './location/location.component';
import { AddCountryComponent } from './add-country/add-country.component';
// import { ViewLocationComponent } from './view-location/view-location.component';
// import { ViewFareComponent } from './view-fare/view-fare.component';
import { AddStateComponent } from './add-state/add-state.component';
import { AddCityComponent } from './add-city/add-city.component';


const routes: Routes = [
      { path: '', component: LocationComponent},
    { path: 'add_country', component: AddCountryComponent},
    { path: 'add_state', component: AddStateComponent},
    { path: 'add_city', component: AddCityComponent}
  
    // { path: 'view-fare/:id', component: ViewFareComponent},
    // { path: 'add_country', component: AddCountryComponent},
   
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationMangementRoutingModule {}
