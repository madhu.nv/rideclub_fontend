import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationMangementRoutingModule } from './locationManagement-routing.module';
import { LocationComponent } from './location/location.component';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { AddCountryComponent } from './add-country/add-country.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { ViewLocationComponent } from './view-location/view-location.component';

import { AddStateComponent } from './add-state/add-state.component';
import { AddCityComponent } from './add-city/add-city.component';


@NgModule({
  declarations: [LocationComponent, AddCountryComponent, ViewLocationComponent, AddStateComponent, AddCityComponent],
  imports: [
    CommonModule,
    LocationMangementRoutingModule,
    UiSwitchModule,
    FormsModule ,
    ReactiveFormsModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
  ]
})
export class LocationManagementModule { }
