import { Component, OnInit} from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { ExcelServicesService } from 'src/app/Services/excel/excel-services.service';
import { LocationManagementService } from 'src/app/Services/location-management/location-management.service';
declare var $: any;
@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {
p=1;
searchText;
countries: any;
states: any;
cities: any;
stateCity:any;
id;
selectedLevel;
selectedData;
statesById;
countryUpdateForm : FormGroup = new FormGroup({})
StateUpdateForm: FormGroup = new FormGroup({})
updateCityForm: FormGroup = new FormGroup({})
submitted = false;
stateUpdateSubmit = false;
citySubmitted = false;
excel = [];
stateExcel = [] ;
cityExcel = [] ;
countryObj = {};
stateObj = {};
cityObj = {}
  constructor(private locationService: LocationManagementService,
     private fb: FormBuilder,
     private excelService:ExcelServicesService){
    this.countryUpdateForm = this.fb.group({
     name: new FormControl('', Validators.required),
    status: new FormControl(''),
    id: new FormControl('')
  })
  this.StateUpdateForm = this.fb.group({
    country: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    status: new FormControl(''),
    id: new FormControl('')
   })
   this.updateCityForm = this.fb.group({
  
    name: new FormControl('', Validators.required),
    status: new FormControl(''),
    id: new FormControl('')
   })
  } 
  
  ngOnInit() {
    $(document).ready(function () {
      $('#updateCountry').click(function () {
        $('#countryModal').modal('toggle');
      });
    });
    $(document).ready(function () {
      $('#stateSubmit').click(function () {
        $('#stateModal').modal('toggle');
      });
    });
    $(document).ready(function () {
      $('#submitcity').click(function () {
        $('#cityModal').modal('toggle');
      });
    });
  
    this.getStateById()
    this.getCountry();
    this.getState();
    this.getCity();
    
  }
  getCountry(){
    this.locationService.fetchCountry().subscribe((data:any)=>{
      console.log(data);
      this.countries = data.data
    })
  }
 
  getCity(){
    this.locationService.fetchCity().subscribe((data:any)=>{
      console.log(data);
      this.cities = data.data
    })
  }
  updateModal(country){
    console.log(country)
    this.id = country._id
    console.log(this.id)
    $('#countryModal').modal('toggle');
    this.countryUpdateForm.patchValue({
      name: country.name,
      status: country.status,
      id: country._id
    })
    }
  onSubmit(){
    var data= {
      data:this.countryUpdateForm.value,
      _id:this.id
    }
    this.submitted = true;
    console.log(this.countryUpdateForm.value);
    this.locationService.updateCountry(this.countryUpdateForm.value).subscribe((data)=>{
      console.log(data)
      this.getCountry()
    })
  }
  stateModal(state){
    $('#stateModal').modal('toggle');
    console.log(state)
    this.StateUpdateForm.patchValue({
      country:state.name,
      name: state.name,
      status: state.status,
      id:state._id
    })

  }
  
  onSubmitupdate(){
    this.stateUpdateSubmit = true;
    console.log(this.StateUpdateForm.value)
    this.locationService.updateState(this.StateUpdateForm.value).subscribe((data)=>{
      console.log(data);
      this.getStateById()
    },(err=>{
      console.log(err);
    }))
  }
  get f() {
    return this.StateUpdateForm.controls;
    }
  selected(){
    console.log(this.selectedLevel)
    this.id = this.selectedLevel;
    this.getStateById();
  }
  
  getStateById(){
    console.log(this.id, "mmmmm")
    this.locationService.fetchSingleState(this.id).subscribe((data:any)=>{
      console.log(data)
      this.statesById = data.data
    })
  }
  getState(){
    this.locationService.fetchState().subscribe((data:any)=>{
      this.states =data.data
      console.log(this.states)
    })
  }
  selectedState(){
    console.log(this.selectedData)
    this.getStateCity();
  }
  getStateCity(){
    this.locationService.fetchCityById(this.selectedData).subscribe((data:any)=>{
      console.log(data, "stateCoity");
      this.stateCity = data.data
    })
  }
  updateCityModal(city){
    $('#cityModal').modal('toggle');
    this.updateCityForm.patchValue({
      name: city.name,
      status: city.status,
      id: city._id
    })
  }
  updateSubmitCity(){
    this.citySubmitted = true;
    this.locationService.updateCity(this.updateCityForm.value).subscribe((data)=>{
      console.log(data);
      this.getStateCity()
    },(err=>{
      console.log(err)
     
    }))
  }
  get d() {
    return this.updateCityForm.controls;
    }
  deleteCountry(id){
    console.log(id)
    var r = confirm("Do you really want to delete this record?");
    if(r==true){
      this.locationService.deleteCountry(id).subscribe((data)=>{
        console.log(data)
        this.getCountry();
      },(err=>{
        console.log(err);
      }))
    }else{
      console.log("error")
    }
   }
   deleteState(id){
    console.log(id)
    var r = confirm("Do you really want to delete this record?");
    if(r==true){
      this.locationService.deleteState(id).subscribe((data)=>{
        console.log(data)
        this. getStateById();
      },(err=>{
        console.log(err);
      }))
    }else{
      console.log("error")
    }
   }
   deleteCity(id){
    console.log(id)
    var r = confirm("Do you really want to delete this record?");
    if(r==true){
      this.locationService.deleteCity(id).subscribe((data)=>{
        console.log(data)
        this. getStateCity();
      },(err=>{
        console.log(err);
      }))
    }else{
      console.log("error")
    }
   }
   get fval() {
    return this.countryUpdateForm.controls;
    }
    exportAsXLSX():void {  
      this.countries.forEach(element => {
        this.countryObj = {
          Name: element.name,
          Status: element.status,
          // craetedAt: element.createdAt,
          // UpdatedAt: element.updatedAt
        }
          this.excel.push(this.countryObj)
        
      });
        this.excelService.exportAsExcelFile(this.excel, 'Country');  
     }
     exportStateAsXLSX(): void{
      this.states.forEach(element => {
        this.stateObj = {
          Name: element.name,
          Status: element.status,
          // craetedAt: element.createdAt,
          // UpdatedAt: element.updatedAt
        }
          this.stateExcel.push(this.stateObj)
        
      });
        this.excelService.exportAsExcelFile(this.stateExcel, 'State');  
     }
     exportCityAsXLSX(): void{
      this.cities.forEach(element => {
        console.log(element, "cities")
        this.cityObj = {
          Name: element.name,
          Status: element.status,
          // craetedAt: element.createdAt,
          // UpdatedAt: element.updatedAt
        }
          this.cityExcel.push(this.cityObj)
        
      });
        this.excelService.exportAsExcelFile(this.cityExcel, 'City');  
     }
}
