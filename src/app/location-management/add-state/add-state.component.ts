import { Component, OnInit } from '@angular/core';
import { LocationManagementService } from 'src/app/Services/location-management/location-management.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-state',
  templateUrl: './add-state.component.html',
  styleUrls: ['./add-state.component.css']
})
export class AddStateComponent implements OnInit {
  countries: any;
  selectedLevel;
  submitted = false;
  addStateForm: FormGroup = new FormGroup({})
  constructor(private locationService: LocationManagementService, private fb: FormBuilder, private toastr: ToastrService, private route: ActivatedRoute,private router: Router) { 
    this.addStateForm = this.fb.group({
      country: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      status: new FormControl(''),
     })
  }

  ngOnInit() {
    this.getCountry()
  }

  //Get countries list
  getCountry(){
    this.locationService.fetchCountry().subscribe((data:any)=>{
      console.log(data);
      this.countries = data.data
    })
  }
  selected(){
    console.log(this.selectedLevel)
  }
  get fval() {
    return this.addStateForm.controls;
    }
  onSubmit(){
    this.submitted = true;
  console.log(this.addStateForm.value)
    this.locationService.createState(this.addStateForm.value).subscribe((data)=>{
      console.log(data);
      this.addStateForm.reset();
      this.router.navigate(['/side-menu',{outlets:{sidebar:'location'}}], {relativeTo: this.route})
    },(err=>{
      this.toastr.error(`${err.error.message}`)
    }))
  }
  
}
