import { Component, OnInit } from '@angular/core';
import { LocationManagementService } from 'src/app/Services/location-management/location-management.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.css']
})
export class AddCountryComponent implements OnInit {
  addCountryForm: FormGroup = new FormGroup({});
  submitted = false;
  constructor( private locationService: LocationManagementService, private fb: FormBuilder,  private toastr: ToastrService, private route: ActivatedRoute,private router: Router) { 
     this.addCountryForm = this.fb.group({
      name: new FormControl('', Validators.required),
      status: new FormControl(''),
     })
    
  }

  ngOnInit() {}
  get fval() {
    return this.addCountryForm.controls;
    }
  onSubmit(){
    this.submitted = true;
    console.log(this.addCountryForm.value);
    this.locationService.createCountry(this.addCountryForm.value).subscribe((data)=>{
      console.log(data, "country data");
      this.addCountryForm.reset()
      this.router.navigate(['/side-menu',{outlets:{sidebar:'location'}}], {relativeTo: this.route})
    },(err=>{
      console.log(err)
      this.toastr.error( `${err.error.message}`);
    }))
  }
 
 

  
}
