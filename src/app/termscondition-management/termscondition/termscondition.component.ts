import { Component, OnInit } from '@angular/core';
import { TermsconditionmanagementService } from 'src/app/Services/termsconditionManagement/termsconditionmanagement.service';

@Component({
  selector: 'app-termscondition',
  templateUrl: './termscondition.component.html',
  styleUrls: ['./termscondition.component.css']
})
export class TermsconditionComponent implements OnInit {
  fetchTermscArray: any;
  id:any;
  constructor( private termsconditionService: TermsconditionmanagementService) { }

  ngOnInit() {
    this.fetchTermscData()
  }

  fetchTermscData(){
    this.termsconditionService.fetchTermsCondition().subscribe((data: any)=>{
      this.fetchTermscArray = data.response[0].terms_condition_data
      this.id = data.response[0]._id
      console.log(this.id)
      console.log(this.fetchTermscArray, "terms condition data")
    }, (err)=>{
      console.log(err)
    })
  }
}
