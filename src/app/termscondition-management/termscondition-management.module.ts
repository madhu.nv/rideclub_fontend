import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TermsConditionManagementRoutingModule } from './termscondition-routing.module';
import { TermsconditionComponent } from './termscondition/termscondition.component';
import { UpdateTermsConditionComponent } from './update-terms-condition/update-terms-condition.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ViewTermsConditionComponent } from './view-terms-condition/view-terms-condition.component';



@NgModule({
  declarations: [TermsconditionComponent, UpdateTermsConditionComponent, ViewTermsConditionComponent],
  imports: [
    CommonModule,
    TermsConditionManagementRoutingModule,
    FormsModule ,
    ReactiveFormsModule,
    CKEditorModule
  ]
})
export class TermsconditionManagementModule { }
