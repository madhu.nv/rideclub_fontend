import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TermsconditionComponent } from './termscondition/termscondition.component';
import { UpdateTermsConditionComponent } from './update-terms-condition/update-terms-condition.component';
import { ViewTermsConditionComponent } from './view-terms-condition/view-terms-condition.component';

const routes: Routes = [
    { path: "", component:TermsconditionComponent},
    { path: "update_terms_condition/:id", component: UpdateTermsConditionComponent},
    { path: "view_terms_condition/:id", component: ViewTermsConditionComponent}
   
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TermsConditionManagementRoutingModule { }