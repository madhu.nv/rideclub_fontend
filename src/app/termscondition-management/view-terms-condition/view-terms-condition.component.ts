import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TermsconditionmanagementService } from 'src/app/Services/termsconditionManagement/termsconditionmanagement.service';

@Component({
  selector: 'app-view-terms-condition',
  templateUrl: './view-terms-condition.component.html',
  styleUrls: ['./view-terms-condition.component.css']
})
export class ViewTermsConditionComponent implements OnInit {
  termsc_id:any;
  fetchTermscArray: any;
  constructor(private termsConditionService: TermsconditionmanagementService, private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.termsc_id = this.actRoute.snapshot.params.id
    console.log(this.termsc_id, "termsc_id")
    this.fetchData()
  }
  fetchData(){
    this.termsConditionService.fetchTermscById(this.termsc_id).subscribe((data:any)=>{
      console.log(data)
      this.fetchTermscArray = data.response.terms_condition_data
    })
  }
}
