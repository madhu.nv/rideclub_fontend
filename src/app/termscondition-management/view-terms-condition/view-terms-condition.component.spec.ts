import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTermsConditionComponent } from './view-terms-condition.component';

describe('ViewTermsConditionComponent', () => {
  let component: ViewTermsConditionComponent;
  let fixture: ComponentFixture<ViewTermsConditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTermsConditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTermsConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
