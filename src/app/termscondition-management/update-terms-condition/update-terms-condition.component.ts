import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { TermsconditionmanagementService } from 'src/app/Services/termsconditionManagement/termsconditionmanagement.service';

@Component({
  selector: 'app-update-terms-condition',
  templateUrl: './update-terms-condition.component.html',
  styleUrls: ['./update-terms-condition.component.css']
})
export class UpdateTermsConditionComponent implements OnInit {
  termsc_id:any;
  editTermsCDataForm: FormGroup = new FormGroup({})
  public Editor = ClassicEditor;
  ckeConfig: any;  
  fetchTermscDataArray: any;
  submitted = false;
  constructor(private actRoute: ActivatedRoute, private fb: FormBuilder, private router: Router, private termsconditionService: TermsconditionmanagementService) { 
    this.editTermsCDataForm = this.fb.group({
      terms_condition_data: new FormControl('', Validators.required),
     
    })
  }

  ngOnInit() {
    this.termsc_id = this.actRoute.snapshot.params.id
    console.log(this.termsc_id, "terms condition Id")
    this.fetchHelpDataById()
  }

  fetchHelpDataById(){
    this.termsconditionService.fetchTermscById(this.termsc_id).subscribe((data:any)=>{
      this.fetchTermscDataArray = data.response.terms_condition_data
      console.log(this.fetchTermscDataArray)
      this.editTermsCDataForm.patchValue({
        terms_condition_data : this.fetchTermscDataArray
      })
    },(err=>{
      console.log(err)
    }))
  }

  get f() { return this.editTermsCDataForm.controls}

  onSubmit(){
    this.submitted = true;
    this.termsconditionService.updateTermsCondition(this.editTermsCDataForm.value).subscribe((data)=>{
      console.log(data, "update help data")
      this.editTermsCDataForm.reset()
      this.router.navigate(['/side-menu',{outlets:{sidebar:'termscondition'}}], {relativeTo: this.actRoute})
    }, (err=>{
      console.log(err)
    }))
  }
}
