import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateTermsConditionComponent } from './update-terms-condition.component';

describe('UpdateTermsConditionComponent', () => {
  let component: UpdateTermsConditionComponent;
  let fixture: ComponentFixture<UpdateTermsConditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateTermsConditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateTermsConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
