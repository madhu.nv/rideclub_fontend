import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromocodeComponent } from './promocode/promocode.component';
import { PromocodeMangementRoutingModule } from './promocodeManagement-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddPromocodeComponent } from './add-promocode/add-promocode.component';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { EditPromocodeComponent } from './edit-promocode/edit-promocode.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [PromocodeComponent, AddPromocodeComponent, EditPromocodeComponent],
  imports: [
    CommonModule,
    PromocodeMangementRoutingModule,
    FormsModule ,
    ReactiveFormsModule,
    UiSwitchModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
  ]
})
export class PromocodeManagementModule {}
