import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPromocodeComponent } from './add-promocode/add-promocode.component';
import { EditPromocodeComponent } from './edit-promocode/edit-promocode.component';
import { PromocodeComponent } from './promocode/promocode.component';

const routes: Routes = [
    { path: '', component: PromocodeComponent},
    { path: 'add_promocode', component: AddPromocodeComponent},
    { path: 'edit_promocode/:id', component: EditPromocodeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromocodeMangementRoutingModule {}
