import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PromocodeManagementService } from 'src/app/Services/promocodeManagement/promocode-management.service';
declare var $: any;

@Component({
  selector: 'app-add-promocode',
  templateUrl: './add-promocode.component.html',
  styleUrls: ['./add-promocode.component.css']
})
export class AddPromocodeComponent implements OnInit {
  promocodeForm: FormGroup = new FormGroup({});
  submitted = false;
  todayDate;
  constructor(private fb: FormBuilder, 
    private promocodeService: PromocodeManagementService,
    private toastr: ToastrService,
    private actRoute: ActivatedRoute,
    private router: Router,
    ) { 
    this.promocodeForm = this.fb.group({
      codeName: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required),
      usage_limit: new FormControl('', Validators.required),
      start_date: new FormControl('', Validators.required),
      end_date:  new FormControl('', Validators.required),
      status:  new FormControl('', Validators.required)
     })
  }

  ngOnInit() { 
    var datePipe=new DatePipe("en-US");
    this.todayDate=datePipe.transform(new Date(), 'yyyy-MM-dd');}
  get f() { return this.promocodeForm.controls; }
  onSubmit(){
    this.submitted = true;
    this.promocodeService.createPromoCode(this.promocodeForm.value).subscribe((data)=>{
      console.log(data);
      this.toastr.success("Submitted Successfully")
      this.promocodeForm.reset()
      this.router.navigate(['/side-menu',{outlets:{sidebar:'promocode'}}], {relativeTo: this.actRoute})
    },(err=>{
      console.log(err);
      this.toastr.error(`${err.error.message}`)
    }))
  }

}
