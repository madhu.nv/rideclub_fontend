import { Component, OnInit } from '@angular/core';
import { ExcelServicesService } from 'src/app/Services/excel/excel-services.service';
import { PromocodeManagementService } from 'src/app/Services/promocodeManagement/promocode-management.service';

@Component({
  selector: 'app-promocode',
  templateUrl: './promocode.component.html',
  styleUrls: ['./promocode.component.css']
})
export class PromocodeComponent implements OnInit {
promocodes = [] ;
p=1;
searchText;
excel = [];
obj = {}
  constructor( private promocodeService: PromocodeManagementService, 
    private excelService:ExcelServicesService) { }

  ngOnInit() {
    this.fetchPromocodeData();
  }
    fetchPromocodeData(){
      this.promocodeService.fetchPromocode().subscribe((data: any)=>{
        console.log(data);
        this.promocodes = data.response
      },(err=>{
        console.log(err);
      }))
    }
    delete(id){
      var r = confirm("Do you really want to delete this record?");
      if(r==true){
        this.promocodeService.deletePromocode(id).subscribe((data)=>{
          console.log(data);
          this.fetchPromocodeData();
        },(err=>{
          console.log(err);
        }))
      }else{
        console.log("error")
      }
     
    }
    exportAsXLSX():void {  
      this.promocodes.forEach(element => {
        this.obj = {
          CodeName : element.codeName,
          End_date: element.end_date,
          Start_date: element.start_date,
          Status: element.status,
          Type: element.type,
          Usage_limit: element.usage_limit,
          Value: element.value,
          CreatedAt: element.createdAt,
          UpdatedAt: element.updatedAt
        }
        this.excel.push(this.obj)
      });
        this.excelService.exportAsExcelFile(this.excel, 'promocodes');  
     }
}
