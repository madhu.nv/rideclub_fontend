import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PromocodeManagementService } from 'src/app/Services/promocodeManagement/promocode-management.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-edit-promocode',
  templateUrl: './edit-promocode.component.html',
  styleUrls: ['./edit-promocode.component.css']
})
export class EditPromocodeComponent implements OnInit {
  id : String
  promocodes = [] ;
  codeName
  type
  value
  usage_limit
  start_date
  end_date
  status
  myStartDateFormat
  expiredDateFormat
  updatepromocodeForm: FormGroup = new FormGroup({})
  constructor(
    private fb: FormBuilder, 
    private promocodeService: PromocodeManagementService,
    private toastr: ToastrService,
    private actRoute: ActivatedRoute,
    private router: Router,
    // private datePipe: DatePipe
  ) { 
    this.updatepromocodeForm = this.fb.group({
      codeName: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      value: new FormControl('', Validators.required),
      usage_limit: new FormControl('', Validators.required),
      start_date: new FormControl('', Validators.required),
      end_date:  new FormControl('', Validators.required),
      status:  new FormControl('', Validators.required),
      id: new FormControl('')
     })
  }

  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "id");
    this.fetchSinglePromocodeData()
  }

  fetchSinglePromocodeData(){
    this.promocodeService.fetchSinlePromocode(this.id).subscribe((data: any)=>{
      console.log(data);
    this.codeName =  data.response.codeName,
    this.type = data.response.type,
    this.value =  data.response.value
    this.usage_limit =  data.response.usage_limit
    this.start_date =  data.response.start_date
    this.end_date =  data.response.end_date
   this.status =  data.response.status
   var datePipe=new DatePipe("en-US");
   this.myStartDateFormat = datePipe.transform(this.start_date, 'yyyy-MM-dd');
  this.expiredDateFormat = datePipe.transform(this.end_date, 'yyyy-MM-dd');
      this.updatepromocodeForm.patchValue({
        codeName: this.codeName,
        type : this.type,
        value : this.value,
        usage_limit : this.usage_limit,
        start_date : this.myStartDateFormat,
        end_date : this.expiredDateFormat,
        status : this.status,
        id: this.id
      })
    },(err=>{
      console.log(err);
    }))
  }
  onSubmit(){
    this.promocodeService.updatePromoCode(this.updatepromocodeForm.value).subscribe((data)=>{
      console.log(data);
      this.toastr.success("Submitted Successfully")
      this.updatepromocodeForm.reset()
      this.router.navigate(['/side-menu',{outlets:{sidebar:'promocode'}}], {relativeTo: this.actRoute})
    },(err=>{
      console.log(err)
    }))
  }
  get f() { return this.updatepromocodeForm.controls; }
}
