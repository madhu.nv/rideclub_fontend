import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RiderDetailsComponent } from './rider-details/rider-details.component';
import { RiderMangementRoutingModule } from './ridermanagement-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RiderKycComponent } from './rider-kyc/rider-kyc.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';



@NgModule({
  declarations: [RiderDetailsComponent, RiderKycComponent],
  imports: [
    CommonModule,
    RiderMangementRoutingModule,
    NgxPaginationModule,
    UiSwitchModule,
    FormsModule ,
    ReactiveFormsModule ,
    Ng2SearchPipeModule 
  ]
})
export class RiderManagementModule { }
