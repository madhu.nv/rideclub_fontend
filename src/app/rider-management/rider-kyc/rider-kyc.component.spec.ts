import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiderKycComponent } from './rider-kyc.component';

describe('RiderKycComponent', () => {
  let component: RiderKycComponent;
  let fixture: ComponentFixture<RiderKycComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiderKycComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiderKycComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
