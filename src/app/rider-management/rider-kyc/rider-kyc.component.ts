import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RiderManagementService } from 'src/app/Services/riderManagement/rider-management.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-rider-kyc',
  templateUrl: './rider-kyc.component.html',
  styleUrls: ['./rider-kyc.component.css']
})
export class RiderKycComponent implements OnInit {
 rider_docId_id : string
 getKycRiderArray = []
 getVehicleArray : any;
 govtID : any;
 imageData: any;
 drivingLicense: any;
 drivingLicenseDoc: any;
 riderKycDataArray: any
 vehicleInfo: any = [];
 govtIdverifStatus;
 dlIdverifStatus ;
 riderGovtIdapprovalForm: FormGroup = new FormGroup({})
 riderRejectGovtIdForm: FormGroup = new FormGroup({})
 approvalDrivingLicenseForm: FormGroup = new FormGroup({})
 rejectDlForm: FormGroup = new FormGroup({})
  constructor( private actRoute: ActivatedRoute, private riderService: RiderManagementService, private fb: FormBuilder, private toastr: ToastrService) { 
    this.rider_docId_id  = this.actRoute.snapshot.params.uid;
    console.log(this.rider_docId_id , "docID")

    this.riderGovtIdapprovalForm = this.fb.group({
      govtIdVerifStatus: new FormControl(1),
      type: new FormControl('govtid'),
      uid :new FormControl(`${this.rider_docId_id}`)
    })

     //Reject GovtId status
     this.riderRejectGovtIdForm = this.fb.group({
      govtIdVerifStatus: new FormControl(0),
      type: new FormControl('govtid'),
      uid :new FormControl(`${this.rider_docId_id}`)
    })

    //DL approve status
    this.approvalDrivingLicenseForm = this.fb.group({
      drivingLicenseDocVerifStatus: new FormControl(1),
      type: new FormControl('drivinglic'),
      uid :new FormControl(`${this.rider_docId_id}`)
    })

    //Dl Reject status
    this.rejectDlForm = this.fb.group({
      drivingLicenseDocVerifStatus: new FormControl(0),
      type: new FormControl('drivinglic'),
      uid :new FormControl(`${this.rider_docId_id}`)
    })
   
  }

  ngOnInit() {
    // this.getVehicleInfo(this.rider_docId_id)
    this.getKycRider(this.rider_docId_id)
    this.getRiderSingle()
  }

  // getVehicleInfo(rider_docId_id){
  //   this.riderService.fetchVehicleInfo(rider_docId_id).subscribe((vehicleData:any)=>{
  //     this.getVehicleArray = vehicleData.data[0]
  //     console.log(this.getVehicleArray, "vehicle data getting successfully")
  //   })
  // }

  getKycRider(rider_docId_id){
  this.riderService.fetchRiderKyc(rider_docId_id).subscribe((riderKycData:any)=>{
 this.riderKycDataArray = riderKycData.data;
 console.log(this.riderKycDataArray, "fbjghkgjkgh")
  // console.log(this.riderKycDataArray[0].vehicleInfo, "data")
  this.vehicleInfo = this.riderKycDataArray.vehicleInfo
   
  this.imageData = this.riderKycDataArray.govtProof.govtIdDoc
  this.govtID = this.riderKycDataArray.govtProof.govtId
    this.drivingLicense = this.riderKycDataArray.vehicleProof.drivingLicense
    this.drivingLicenseDoc = this.riderKycDataArray.vehicleProof.drivingLicenseDoc
    
  })
  }

  approvalRiderGovtStatus(riderGovtIdapprovalForm){
    var govtIdVerifStatus = riderGovtIdapprovalForm.value.govtIdVerifStatus;
   var type = riderGovtIdapprovalForm.value.type;
   var uid = riderGovtIdapprovalForm.value.uid;
    this.riderService.RiderApprovalReject({ "govtIdVerifStatus": govtIdVerifStatus,"type": type,"uid":uid }).subscribe(approvalData=>{
      console.log(approvalData, "approvaldta")
      this.toastr.success('Approved');
    })
  }

  rejectGovtIdStatus(riderRejectGovtIdForm){
    console.log(riderRejectGovtIdForm.value)
    var govtIdVerifStatus = riderRejectGovtIdForm.value.govtIdVerifStatus;
     var type = riderRejectGovtIdForm.value.type;
     var uid = riderRejectGovtIdForm.value.uid;
      this.riderService.RiderApprovalReject({ "govtIdVerifStatus": govtIdVerifStatus,"type": type,"uid":uid }).subscribe(approvalData=>{
        console.log(approvalData, "rejectData")
        this.toastr.error( 'Rejected');
      })
  }

  approvalDLStatus(approvalDrivingLicenseForm){
    console.log(approvalDrivingLicenseForm)
    var drivingLicenseDocVerifStatus = approvalDrivingLicenseForm.value.drivingLicenseDocVerifStatus;
    var type = approvalDrivingLicenseForm.value.type;
    var uid = approvalDrivingLicenseForm.value.uid;
    this.riderService.postRiderDl({ "drivingLicenseDocVerifStatus": drivingLicenseDocVerifStatus,"type": type,"uid":uid }).subscribe((dlApproval)=>{
      console.log(dlApproval, "DL Approval")
      this.toastr.success('Approved');
    })
  }

  rejectDlStatus(rejectDlForm){
    console.log(rejectDlForm)
    var drivingLicenseDocVerifStatus = rejectDlForm.value.drivingLicenseDocVerifStatus;
    var type = rejectDlForm.value.type;
    var uid = rejectDlForm.value.uid;
    this.riderService.postRiderDl({ "drivingLicenseDocVerifStatus": drivingLicenseDocVerifStatus,"type": type,"uid":uid }).subscribe((dlReject)=>{
      console.log(dlReject, "DL Approval")
      this.toastr.error( 'Rejected');
    })
  }
    getRiderSingle(){
      this.riderService.getRiderSingleData(this.rider_docId_id ).subscribe((data: any)=>{
          console.log(data);
          this.govtIdverifStatus = data.data.govtIdVerifStatus,
          this.dlIdverifStatus = data.data.drivingLicenseDocVerifStatus
          this.getKycRider(this.rider_docId_id)
          this.getRiderSingle()
      }, (err=>{
        console.log(err);
      }))
    }
 
}
