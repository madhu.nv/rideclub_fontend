import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RiderDetailsComponent } from './rider-details/rider-details.component';
import { RiderKycComponent } from './rider-kyc/rider-kyc.component';

const routes: Routes = [
    { path: '', component: RiderDetailsComponent},
    { path: 'rider/:uid', component: RiderKycComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RiderMangementRoutingModule { }
