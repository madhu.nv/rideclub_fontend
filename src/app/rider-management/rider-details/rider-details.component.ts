import { Component, OnInit } from '@angular/core';
import { RiderManagementService } from 'src/app/Services/riderManagement/rider-management.service';
import { ExcelServicesService } from 'src/app/Services/excel/excel-services.service';

@Component({
  selector: 'app-rider-details',
  templateUrl: './rider-details.component.html',
  styleUrls: ['./rider-details.component.css']
})
export class RiderDetailsComponent implements OnInit {
  p=1
  getRiderArray:any;
  searchText;
  excel = [];
  constructor( private riderService: RiderManagementService, private excelService:ExcelServicesService) { this.fetchRiderData()}

  ngOnInit() {
  }

  fetchRiderData(){
    this.riderService.fetchRider().subscribe((riders:any)=>{
      console.log(riders)
     this.getRiderArray = riders.data
     console.log(this.getRiderArray)
    })
  }

  getUrl(filename){
    console.log(filename, "hh")
    let image = "http://rider-club.dxminds.online/rider_club_backend/Public/user/profilepic/"+ filename;
    return image;
  }

  exportAsXLSX():void {  
    this.getRiderArray.forEach(element => {
      console.log(element)
      this.excel.push(element)
    
    });
   
      this.excelService.exportAsExcelFile(this.excel, 'Riders');  
   }


  deleteRiderFirebase(riderData) {
    console.log(riderData)
   var r = confirm("Do you really want to delete this record?");
   if(r == true){
    this.riderService.deleteRider(riderData).subscribe(deletData=>{
      console.log(deletData)
      this.fetchRiderData()
    })
   }else{
     console.log("cancelled")
   }
   
    
  }

}
