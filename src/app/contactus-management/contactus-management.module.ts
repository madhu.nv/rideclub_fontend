import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactusMangementRoutingModule } from './contactusManagement-routing.module';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';



@NgModule({
  declarations: [ContactUsComponent],
  imports: [
    CommonModule,
    ContactusMangementRoutingModule,
    UiSwitchModule,
    FormsModule ,
    ReactiveFormsModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
  ]
})
export class ContactusManagementModule { }
