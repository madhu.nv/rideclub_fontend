import { Component, OnInit } from '@angular/core';
import { ContactusManagementService } from 'src/app/Services/contactusManagement/contactus-management.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ExcelServicesService } from 'src/app/Services/excel/excel-services.service';
declare var $: any;

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
contactusArray = [];
updateStatusForm: FormGroup = new FormGroup({})
searchText;
p=1;
contactusObj = {};
excel = [] ;
  constructor( private contactService: ContactusManagementService, 
    private fb: FormBuilder,
    private excelService:ExcelServicesService) { 
    this.updateStatusForm = this.fb.group({
      status: new FormControl(''),
      id: new FormControl('')
     })
  }

  ngOnInit() {
    $(document).ready(function () {
      $('#submitModal').click(function () {
        $('#statusUpdateModal').modal('toggle');
      });
    });
    this.fetchContactData();
  }
fetchContactData(){
  this.contactService.fetchContactUs().subscribe((data:any)=>{
    console.log(data);
    this.contactusArray = data.response;
  },(err=>{
    console.log(err);
  }))
}
updateModal(contactus){
  $('#statusUpdateModal').modal('toggle');
  console.log(contactus);
  this.updateStatusForm.patchValue({
    id: contactus._id,
    status: contactus.status
  })
}
onSubmit(){
  console.log(this.updateStatusForm.value)
  this.contactService.updateContactUs(this.updateStatusForm.value).subscribe((data)=>{
    this.fetchContactData();
  },(err=>{
    console.log(err);
  }))
}
exportAsXLSX():void {  
  this.contactusArray.forEach(element => {
    this.contactusObj = {
      Name: element.name,
      Email: element.email,
      MobileNumber: element.phoneNum,
      Feedback: element.feedback,
      Status: element.status,
     
    }
      this.excel.push(this.contactusObj)
    
  });
    this.excelService.exportAsExcelFile(this.excel, 'ContactUs');  
 }
}
