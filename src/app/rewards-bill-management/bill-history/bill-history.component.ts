import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BillVerificationService } from 'src/app/Services/rewards-bill-management/bill-verification.service';

@Component({
  selector: 'app-bill-history',
  templateUrl: './bill-history.component.html',
  styleUrls: ['./bill-history.component.css']
})
export class BillHistoryComponent implements OnInit {
  id : String;
  documentArray = [];
  vehicleType;
  vehicleStatus;
  url
  constructor(  private actRoute: ActivatedRoute, private billService: BillVerificationService) { }

  ngOnInit() {
    this.id = this.actRoute.snapshot.params.uid
    console.log(this.id, "id");
    this.fetchSingleData()
  }

  fetchSingleData(){
    this.billService.fetchSingleDocument(this.id).subscribe((data: any)=>{
      console.log(data.response);
      this.documentArray = data.response
      this.vehicleType = data.response.vehicletype,
      this.vehicleStatus = data.response.documentverifiedstatus,
      this.url = data.response.claimdocument.url
    },(err=>{
      console.log(err)
    }))
  }

}
