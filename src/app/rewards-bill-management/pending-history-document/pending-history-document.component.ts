import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BillVerificationService } from 'src/app/Services/rewards-bill-management/bill-verification.service';

@Component({
  selector: 'app-pending-history-document',
  templateUrl: './pending-history-document.component.html',
  styleUrls: ['./pending-history-document.component.css']
})
export class PendingHistoryDocumentComponent implements OnInit {
  id : String;
  vehicleType;
  document;
  constructor(private actRoute: ActivatedRoute, private billService: BillVerificationService) { }

  ngOnInit() {
   
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "id");
    this.fetchSinglePendingDocumnet()
  }

  fetchSinglePendingDocumnet(){
    this.billService.fetchSinglependingData(this.id).subscribe((data: any)=>{
      console.log(data);
      this.vehicleType = data.response.vehicletype
      this.document = data.response.claimdocument.url
    },(err=>{
      console.log(err)
    }))
  }

}
