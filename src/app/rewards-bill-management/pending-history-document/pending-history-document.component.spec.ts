import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingHistoryDocumentComponent } from './pending-history-document.component';

describe('PendingHistoryDocumentComponent', () => {
  let component: PendingHistoryDocumentComponent;
  let fixture: ComponentFixture<PendingHistoryDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingHistoryDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingHistoryDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
