import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RewardsBillComponent } from './rewards-bill/rewards-bill.component';
import { RewardsBillMangementRoutingModule } from './rewardsBillManagement-routing.module';
import { BillHistoryComponent } from './bill-history/bill-history.component';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PendingHistoryDocumentComponent } from './pending-history-document/pending-history-document.component';

@NgModule({
  declarations: [RewardsBillComponent, BillHistoryComponent, PendingHistoryDocumentComponent],
  imports: [
    CommonModule,
    RewardsBillMangementRoutingModule,
    UiSwitchModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    FormsModule ,
    ReactiveFormsModule,
  ]
})
export class RewardsBillManagementModule { }
