import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillHistoryComponent } from './bill-history/bill-history.component';
import { PendingHistoryDocumentComponent } from './pending-history-document/pending-history-document.component';
import { RewardsBillComponent } from './rewards-bill/rewards-bill.component';


const routes: Routes = [
    { path: '', component: RewardsBillComponent},
    { path: 'bill_history/:uid', component: BillHistoryComponent},
    { path: 'pending_history/:id', component: PendingHistoryDocumentComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RewardsBillMangementRoutingModule {}
