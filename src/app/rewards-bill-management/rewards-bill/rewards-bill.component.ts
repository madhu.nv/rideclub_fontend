import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ExcelServicesService } from 'src/app/Services/excel/excel-services.service';
import { BillVerificationService } from 'src/app/Services/rewards-bill-management/bill-verification.service';

declare var $: any;

@Component({
  selector: 'app-rewards-bill',
  templateUrl: './rewards-bill.component.html',
  styleUrls: ['./rewards-bill.component.css']
})
export class RewardsBillComponent implements OnInit {
  p: number = 1;
  searchText;
  getUserArray = [];
  getPendingArray = [];
  excel = [];
  obj = {}
  pendingExcel = [];
  pendingObj = {};
  updateStatusForm: FormGroup = new FormGroup({})
  constructor(private billService: BillVerificationService, private fb: FormBuilder, private excelService: ExcelServicesService) {
    this.updateStatusForm = this.fb.group({
      documentverifiedstatus: new FormControl('', Validators.required),
      id: new FormControl(''),

    })
  }

  ngOnInit() {

    $(document).ready(function () {
      $('#statusUpdateSubmit').click(function () {
        $('#exampleModal').modal('toggle');
      });
    });

    this.getUserData();
    this.getPendingData();
  }
  getUserData() {
    this.billService.fetchUserData().subscribe((data: any) => {
      console.log(data.response);
      this.getUserArray = data.response
    }, (err => {
      console.log(err);
    }))
  }

  getPendingData() {
    this.billService.fetchPendingDocument().subscribe((data: any) => {
      this.getPendingArray = data.response
      console.log(this.getPendingArray)
    }, (err => {
      console.log(err)
    }))
  }
  updateStatus(pending) {
    $('#exampleModal').modal('toggle');
    console.log(pending)
    this.updateStatusForm.patchValue({
      id: pending._id,
      documentverifiedstatus: pending.documentverifiedstatus,

    })
  }

  onSubmit() {
    console.log(this.updateStatusForm.value)
    this.billService.updatependingDocument(this.updateStatusForm.value).subscribe((data) => {
      console.log(data);
      this.updateStatusForm.reset();
      this.getPendingData()
    }, (err => {
      console.log(err)
    }))
  }

  exportAsXLSX(): void {
    this.getUserArray.forEach(element => {
      this.obj = {
        FirstName: element.firstname,
        Lastname: element.lastname,
        Mobile: element.mobile,
        Organization_Email: element.organizationEmail,
        personal_Email: element.personalEmail
      }
      this.excel.push(this.obj)
    });
    this.excelService.exportAsExcelFile(this.excel, 'History');
  }

  pendingexportAsXLSX() {
    this.getPendingArray.forEach(element => {
      console.log(element)
      this.pendingObj = {
        FirstName: element.firstname,
        Lastname: element.lastname,
        Mobile: element.mobile,
        Organization_Email: element.organizationEmail,
        Personal_Email: element.personalEmail,
        Vehicletype: element.vehicletype,
        Document_verified_status: element.documentverifiedstatus
      }
      this.pendingExcel.push(this.pendingObj)
    });
    this.excelService.exportAsExcelFile(this.pendingExcel, 'pendingData');
  }
}
