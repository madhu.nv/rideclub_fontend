import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardsBillComponent } from './rewards-bill.component';

describe('RewardsBillComponent', () => {
  let component: RewardsBillComponent;
  let fixture: ComponentFixture<RewardsBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewardsBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardsBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
