import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingHistoryComponent } from './booking-history/booking-history.component';
import { BookingMangementRoutingModule } from './bookingManagement-routing.module';
import { BookingLogDetailsComponent } from './booking-log-details/booking-log-details.component';
import { BookingDetailsComponent } from './booking-details/booking-details.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { SchedulerecurrDetailsComponent } from './schedulerecurr-details/schedulerecurr-details.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CurrofferrideDetailsComponent } from './currofferride-details/currofferride-details.component';
import { SchrecurrOfferrideDetailsComponent } from './schrecurr-offerride-details/schrecurr-offerride-details.component';



@NgModule({
  declarations: [BookingHistoryComponent, BookingLogDetailsComponent, BookingDetailsComponent, SchedulerecurrDetailsComponent, CurrofferrideDetailsComponent, SchrecurrOfferrideDetailsComponent],
  imports: [
    CommonModule,
    BookingMangementRoutingModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    FormsModule ,
    ReactiveFormsModule ,
  ]
})
export class BookingManagementModule { }
