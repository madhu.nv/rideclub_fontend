import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchrecurrOfferrideDetailsComponent } from './schrecurr-offerride-details.component';

describe('SchrecurrOfferrideDetailsComponent', () => {
  let component: SchrecurrOfferrideDetailsComponent;
  let fixture: ComponentFixture<SchrecurrOfferrideDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchrecurrOfferrideDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchrecurrOfferrideDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
