import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookingmanagementService } from 'src/app/Services/bookingmanagement/bookingmanagement.service';

@Component({
  selector: 'app-schrecurr-offerride-details',
  templateUrl: './schrecurr-offerride-details.component.html',
  styleUrls: ['./schrecurr-offerride-details.component.css']
})
export class SchrecurrOfferrideDetailsComponent implements OnInit {
  id : String;
  vehicletype;
  brand;
  color;
  fueltype;
  charge_km;
  helmet;
  seat;
  registrationNumber
  travellerArray: any;
  constructor(private actRoute: ActivatedRoute, private bookingService: BookingmanagementService) { 
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "Id");
  }

  ngOnInit() {
    this.fetchschRecurDetails()
  }
  fetchschRecurDetails(){
    this.bookingService.fetchScheOfferRideDetails(this.id).subscribe((data:any)=>{
      console.log(data.response , "schedggjh");
      this.travellerArray = data.response.travellers
      this.vehicletype = data.response.vehicleInfo[0].vehicletype
      this.brand = data.response.vehicleInfo[0].brand
      this.fueltype = data.response.vehicleInfo[0].fuelType
      this.charge_km = data.response.vehicleInfo[0].charge_KM
      this.helmet = data.response.vehicleInfo[0].helmet
      this.seat = data.response.vehicleInfo[0].seat
      this.color = data.response.vehicleInfo[0].color
      this.registrationNumber = data.response.vehicleInfo[0].registrationNumber
    },(err=>{
      console.log(err)
    }))
  }
}
