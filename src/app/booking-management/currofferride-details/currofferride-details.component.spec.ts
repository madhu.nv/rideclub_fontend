import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrofferrideDetailsComponent } from './currofferride-details.component';

describe('CurrofferrideDetailsComponent', () => {
  let component: CurrofferrideDetailsComponent;
  let fixture: ComponentFixture<CurrofferrideDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrofferrideDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrofferrideDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
