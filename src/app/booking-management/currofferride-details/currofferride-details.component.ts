import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookingmanagementService } from 'src/app/Services/bookingmanagement/bookingmanagement.service';

@Component({
  selector: 'app-currofferride-details',
  templateUrl: './currofferride-details.component.html',
  styleUrls: ['./currofferride-details.component.css']
})
export class CurrofferrideDetailsComponent implements OnInit {
  id : String;
  travellerArray: any;
  vehicleInfo: any;
  vehicletype;
  brand;
  color;
  fueltype;
  charge_km;
  helmet;
  seat;
  registrationNumber
  constructor(private actRoute: ActivatedRoute, private bookingService: BookingmanagementService) { 
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "Id");
  }

  ngOnInit() {
    this.fetchcurofrData()
  }

  fetchcurofrData(){
    this.bookingService.fetchcurOfrDetails(this.id).subscribe((data:any)=>{
      this.travellerArray = data.response.travellers
      // this.vehicleInfo = data.response.vehicleInfo[0]
      this.vehicletype = data.response.vehicleInfo[0].vehicletype
      this.brand = data.response.vehicleInfo[0].brand
      this.fueltype = data.response.vehicleInfo[0].fuelType
      this.charge_km = data.response.vehicleInfo[0].charge_KM
      this.helmet = data.response.vehicleInfo[0].helmet
      this.seat = data.response.vehicleInfo[0].seat
      this.color = data.response.vehicleInfo[0].color
      this.registrationNumber = data.response.vehicleInfo[0].registrationNumber
    console.log(data.response.vehicleInfo[0])
    },(err=>{
      console.log(err)
    }))
  }

}
