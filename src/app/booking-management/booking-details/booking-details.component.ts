import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookingmanagementService } from 'src/app/Services/bookingmanagement/bookingmanagement.service';

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.css']
})
export class BookingDetailsComponent implements OnInit {
  id : String;
  travellersData = [];
  RidersData = [];
  vehicleData = [];
  constructor(private actRoute: ActivatedRoute, private bookingService: BookingmanagementService) { 
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "docID");
  }

  ngOnInit() {
    this.fetchAllData()
  }


   fetchAllData(){
   this.bookingService.fetchAllDetails(this.id).subscribe((data:any)=>{
     this.travellersData = data.response[0].travDetails
     this.RidersData = data.response[0].riderDetails
     this.vehicleData = data.response[0].vehicle
     console.log(data)
   },(err=>{
     console.log(err)
   }))
 }
}
