import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerecurrDetailsComponent } from './schedulerecurr-details.component';

describe('SchedulerecurrDetailsComponent', () => {
  let component: SchedulerecurrDetailsComponent;
  let fixture: ComponentFixture<SchedulerecurrDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulerecurrDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerecurrDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
