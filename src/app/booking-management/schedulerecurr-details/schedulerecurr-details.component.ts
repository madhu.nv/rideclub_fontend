import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookingmanagementService } from 'src/app/Services/bookingmanagement/bookingmanagement.service';

@Component({
  selector: 'app-schedulerecurr-details',
  templateUrl: './schedulerecurr-details.component.html',
  styleUrls: ['./schedulerecurr-details.component.css']
})
export class SchedulerecurrDetailsComponent implements OnInit {
  id : String;
  scheRecurRideDetails = [] ;
  scheRecurTravDetails = [] ;
  scheRecurVehicle = [] ;
  constructor(private actRoute: ActivatedRoute, private bookingService: BookingmanagementService) { 
    this.id = this.actRoute.snapshot.params.id
    console.log(this.id, "Id");
  }

  ngOnInit() {
    this.scheduleRecurrData()
  }
    scheduleRecurrData(){
      this.bookingService.fetchScheRecurrData(this.id).subscribe((data: any)=>{
        this.scheRecurRideDetails = data.response[0].schedulerideDetails
        this.scheRecurTravDetails = data.response[0].scheduletravDetails
        this.scheRecurVehicle = data.response[0].scheduleRideVehicle
      })
    }
}
