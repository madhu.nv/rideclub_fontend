import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookingHistoryComponent } from './booking-history/booking-history.component';
import { BookingLogDetailsComponent } from './booking-log-details/booking-log-details.component';
import { BookingDetailsComponent } from './booking-details/booking-details.component';
import { SchedulerecurrDetailsComponent } from './schedulerecurr-details/schedulerecurr-details.component';
import { CurrofferrideDetailsComponent } from './currofferride-details/currofferride-details.component';
import { SchrecurrOfferrideDetailsComponent } from './schrecurr-offerride-details/schrecurr-offerride-details.component';


const routes: Routes = [
    { path: '', component: BookingHistoryComponent},
    { path: 'booking_log', component: BookingLogDetailsComponent},
    { path: 'booking_details/:id', component: BookingDetailsComponent},
    { path: 'scherecurbooking_details/:id', component: SchedulerecurrDetailsComponent},
    { path: 'curofferride_details/:id', component: CurrofferrideDetailsComponent},
    { path: 'scherecurr_offerride_details/:id', component: SchrecurrOfferrideDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookingMangementRoutingModule { }
