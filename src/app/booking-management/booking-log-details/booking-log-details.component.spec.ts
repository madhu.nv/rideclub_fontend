import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingLogDetailsComponent } from './booking-log-details.component';

describe('BookingLogDetailsComponent', () => {
  let component: BookingLogDetailsComponent;
  let fixture: ComponentFixture<BookingLogDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingLogDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingLogDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
