import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-booking-log-details',
  templateUrl: './booking-log-details.component.html',
  styleUrls: ['./booking-log-details.component.css']
})
export class BookingLogDetailsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  bookingLogs = [
    {
      riderName: "Vikas",
      reason : "ACCEPTED_BOOKING_COMPLETED_BY_DRIVER",
      created : "09/08/2020 12:08:41 am"
    },
    {
      riderName: "Vikas",
      reason : "BOOKING_STARTED",
      created : "09/08/2020 12:08:41 am"
    },
    {
      riderName: "Vikas",
      reason : "BOOKING_ARRIVED",
      created : "09/08/2020 12:08:41 am"
    },
    {
      riderName: "Vikas",
      reason : "BOOKING_ACCEPTED",
      created : "09/08/2020 12:08:41 am"
    },
  ]
}
