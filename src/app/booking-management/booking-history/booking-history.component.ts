import { Component, OnInit } from '@angular/core';
import { BookingmanagementService } from 'src/app/Services/bookingmanagement/bookingmanagement.service';
import { ExcelServicesService } from 'src/app/Services/excel/excel-services.service';

@Component({
  selector: 'app-booking-history',
  templateUrl: './booking-history.component.html',
  styleUrls: ['./booking-history.component.css']
})
export class BookingHistoryComponent implements OnInit {
  p = 1;
  page = 1;
  pl = 1
  searchText;
  bookingDetails: any;
  scheduleArray = [];
  currentOfferRide: any;
  scheduleOfferRide: any
  excel = [];
  scheduleRecurrexcel = [];
  obj = {};
  scheduleRecurrobj = {};
  curofferobj = {};
  curOfferRide = [];
  schRecurOfrrobj = {};
  schRecurOfferrexcel=[]
  constructor(private bookingService: BookingmanagementService, private excelService: ExcelServicesService) { }

  ngOnInit() {
    this.fetchBookingDetails();
    this.fetchScheduleBooking();
    this.fetchOfferRide();
    this.fetchScheduleRecur()
  }
  fetchBookingDetails() {
    this.bookingService.fetchBookingData().subscribe((data: any) => {
      console.log(data);
      this.bookingDetails = data.response
    }, (err => {
      console.log(err)
    }))
  }

  fetchScheduleBooking() {
    this.bookingService.fetchSchedule().subscribe((data: any) => {
      this.scheduleArray = data.response
    }, (err => {
      console.log(err);
    }))
  }

  exportAsXLSXcurFindRide(): void {
    this.bookingDetails.forEach(element => {
      this.obj = {
        startpointAddress: element.startpointAddress,
        endpointAddress: element.endpointAddress,
        firstname: element.firstname,
        lastname: element.lastname,
        ongoingStatus: element.ongoingStatus,
        date: element.date
      }
      this.excel.push(this.obj)
    });
    this.excelService.exportAsExcelFile(this.excel, 'Current Find ride');
  }

  exportAsXLSXcurSchedulerecurring(): void {
    this.scheduleArray.forEach(element => {
      this.scheduleRecurrobj = {
        startpointAddress: element.startpointAddress,
        endpointAddress: element.endpointAddress,
        firstname: element.firstname,
        lastname: element.lastname,
        ongoingStatus: element.ongoingStatus,
        date: element.date
      }
      this.scheduleRecurrexcel.push(this.scheduleRecurrobj)
    });
    this.excelService.exportAsExcelFile(this.scheduleRecurrexcel, 'Schedule recurring ride');
  }


  fetchOfferRide() {
    this.bookingService.fetchCurrentOfferRide().subscribe((data: any) => {
      this.currentOfferRide = data.response
    }, (err => {
      console.log(err)
    }))
  }

  fetchScheduleRecur() {
    this.bookingService.fetchSechOfferRide().subscribe((data: any) => {
      console.log(data.response);
      this.scheduleOfferRide = data.response
    }, (err => {
      console.log(err)
    }))
  }

  exportAsXLSXcurOfferRide(){
    this.currentOfferRide.forEach(element => {
      this.curofferobj = {
        startpointAddress: element.startpointAddress,
        endpointAddress: element.endpointAddress,
        firstname: element.firstname,
        lastname: element.lastname,
        ongoingStatus: element.ongoingStatus,
        date: element.date
      }
      this.curOfferRide.push(this.curofferobj)
    });
    this.excelService.exportAsExcelFile(this.curOfferRide, 'Current Offer ride');
  }

  exportAsXLSXschOfferRide(){
    this.scheduleOfferRide.forEach(element => {
      this.schRecurOfrrobj = {
        startpointAddress: element.startpointAddress,
        endpointAddress: element.endpointAddress,
        firstname: element.firstname,
        lastname: element.lastname,
        ongoingStatus: element.ongoingStatus,
        date: element.date
      }
      this.schRecurOfferrexcel.push(this.schRecurOfrrobj)
    });
    this.excelService.exportAsExcelFile(this.schRecurOfferrexcel, 'Schedule recurring offer ride');
  }
}
