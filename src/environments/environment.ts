// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: "http://rider-club.dxminds.online:5555",
  firebase : {
    apiKey: "AIzaSyBQAQ2_b88JPvCb2S7r1_OXsl7EOubiYdk",
    authDomain: "riderclubadmin-22474.firebaseapp.com",
    databaseURL: "https://riderclubadmin-22474.firebaseio.com",
    projectId: "riderclubadmin-22474",
    storageBucket: "riderclubadmin-22474.appspot.com",
    messagingSenderId: "1095919617452",
    appId: "1:1095919617452:web:e8774457c1621cf8afe609",
    measurementId: "G-XS2D5S7DD0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
